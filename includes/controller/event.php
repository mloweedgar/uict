<?php

   class EventController extends Controller{
       protected $event;
       public $loader;

       public function __construct(){
           $this->loader = new Loader();
           $this->loader->model('event.php');
           $this->event = new Event();
       }

   	   public function index(){
          $events = $this->event->get_all();
          try{
   	   	 $this->loader->view('events.php',$events);
          }catch(Exception $e){
             echo "Message: ".$e->getMessage();
          }
   	   }

       public function add_event(){
          global $db;
          $this->event->title = $db->db_escape_values($_POST['title']);
          $this->event->description = $db->db_escape_values($_POST['description']);
          $this->event->event_date = $db->db_escape_values($_POST['event_date']);
          $this->event->publisher_id = $db->db_escape_values($_POST['publisher_id']);
          $this->event->category_id = $db->db_escape_values($_POST['category_id']);
          if($this->event->add_project()){
              redirect(URL.'user/all_events');
          }
       }
       
       public function community_event(){
        
         $this->loader->view('community-event.php');
        
       }
       
        public function search(){
	 if(isset($_GET['event_title']) && $_GET['event_title']!=NULL){
	    
	    $events  = $this->event->get_events_by_title($_GET['event_title']);
	    try{
   	       $this->loader->view('events.php',$events);
              }catch(Exception $e){
               echo "Message: ".$e->getMessage();
            }
	    
	 }
	 else if(isset($_GET['begin_date']) && isset($_GET['end_date'])
	    && $_GET['begin_date'] != NULL &&  $_GET['end_date'] != NULL ){
	      $events  = $this->event->get_by_date($_GET['begin_date'],$_GET['end_date']);
	    try{
   	       $this->loader->view('events.php',$events);
              }catch(Exception $e){
               echo "Message: ".$e->getMessage();
            }
	 }
	 else{
	    if(isset($_GET['event_title'])){
	        header('Location:'.URL.'event/index?event_title='.$_GET['event_title'].'');
	    }else if (isset($_GET['begin_date']) ||isset($_GET['end_date'])){
	       if(isset($_GET['begin_date']) && isset($_GET['end_date'])){
		  header('Location:'.URL.'event/index?begin_date='.$_GET['begin_date'].'
			 &end_date='.$_GET['end_date'].''); 
	       }else if(isset($_GET['begin_date'])){
		 header('Location:'.URL.'event/index?begin_date='.$_GET['begin_date'].''); 
	       }
	       else if(isset($_GET['end_date'])){
		 header('Location:'.URL.'event/index?end_date='.$_GET['end_date'].''); 
	       }
	       
	    }else{
	        header('Location:'.URL.'event/index');
	    }
	 exit();
	 }
       }
       
        
       
   }
?>


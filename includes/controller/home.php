<?php



class HomeController extends Controller{
    

    
    public function index(){
        try{
        $this->loader->view('index.php');
        }catch(Exception $e){
            echo 'Message '.$e->getMessage();
        }
    }

    public function about(){
        try{
        $this->loader->view('about.php');
        }catch(Exception $e){
            echo 'Message '.$e->getMessage();
        }
    }

    public function login(){
        try{
        $this->loader->view('login.php');
        }catch(Exception $e){
            echo 'Message '.$e->getMessage();
        }
    }

    public function registration($data=""){
      
        try{
            if(isset($_GET['data'])){
                echo 'data in params'.$_GET['data'];
            }
            $_GET['url'] ='uict_database/home/registration';
            if(isset($data)){
               $this->loader->view('registration.php',$data); 
            }else{
               $this->loader->view('registration.php'); 
            }
            
        }catch(Exception $e){
            echo 'Message '.$e->getMessage();
        }
        
    }

    public function projects(){
        try{
        $this->loader->view('about-projects.php');
        }catch(Exception $e){
            echo 'Message '.$e->getMessage();
        }
    }

    public function charity(){
        try{
        $this->loader->view('about-charity.php');
        }catch(Exception $e){
            echo 'Message '.$e->getMessage();
        }
    }

    public function sports(){
        try{
        $this->loader->view('about-sports.php');
        }catch(Exception $e){
            echo 'Message '.$e->getMessage();
        }
    }
    public function study(){
        try{
        $this->loader->view('about-study.php');
        }catch(Exception $e){
            echo 'Message '.$e->getMessage();
        }
    }
    

    public function userhome($user_id=""){
         $loader = new Loader();
         
         $loader->service('MainService.php');
         $mainService = new MainService();
         $data = $mainService->loadData();
        
       
        try{
        $loader->view('home.php',$data);
        }catch(Exception $e){
            echo 'Message:'.$e->getMessage();
        }
      
    }
   
    public function userProfile($user_id){

         $loader = new Loader();
         
         $loader->service('MainService.php');
         $mainService = new MainService();
         $data = $mainService->loadData();
        
        
        try{
            $loader->view('userProfile.php',$data);
        }catch(Exception $e){
            echo 'Message'.$e->getMessage();
        }
        
    }

    public function editInfo($user_id){

        $user = (new User)->get_user($user_id);

        $loader = new Loader();

        try{
            $loader->view('editInfo.php',$user);
        }catch(Exception $e){
            echo 'Message'.$e->getMessage();
        }
       
    }
    
    public function register(){
        //this if statement checks whether there is post request
        if(!isset($_POST['firstname'])){
        
            header('Location: '.URL.'home/registration');
            exit();
        }
        
        $loader = new Loader();
        $loader->service('MainService.php');
        
        // registering user using mainService 
        $mainService = new MainService();
        
        $user = $mainService->registration();
        
        if($user !=NULL){
            
            if($mainService->validate($user)){
            
          if($user->add_user()){
            
            $member = $mainService->add_user_in_session($user);
            $session = new Session();
            if(isset($member)){
		$session->login($member);
                try{
                $loader->view('welcome.php',$member);
                }catch(Exception $e){
                    echo 'Message'.$e->getMessage();
                }
            }
         }
         }
         else{
            $this->registration();
            exit();
         }
         
        }
        
        else{
            $this->registration();
            exit();
        }
         

    }

    public function editUser(){
        $loader = new Loader();
        $loader->service('MainService.php');
          // editing user using mainService
        $mainService = new MainService();
        
        //session for updating current user attributes
        $session = new Session();
        
        $user = $mainService->registration();
        if($user == NULL){
           $this->editInfo($_SESSION['user_id']);
           exit(); 
        }
        
        //querying all properties into user object
        $dbUser = (new User())->get_user_by_reg_number($user->get_reg_number());
        if($dbUser !=NULL){
          $user->set_id($dbUser->get_id());
        }
        
        /*checking if user has edited password
         *then edit user in database with changed password
         */
        
        if(isset($_POST['password']) && isset($_POST['repeatedPassword'])){
            if($_POST['password'] == $_POST['repeatedPassword']){
                $pass = $_POST['password'];
            }else{
                
            $GLOBALS['registrationError'] = "Passwords do not match";
             $this->editInfo($_SESSION['user_id']);
            exit();   
            }
        }else{
            $pass = NULL;
        }
        
        if($user != NULL){
        if($mainService->validate($user)){
         
         //TODO check why the else statement is the one executed
         if($user->edit_user($_SESSION['user_id'],$pass)){
            $session->login($user);
            
            $_SESSION["message"] =  "Successfully edited profile ";
            header('Location:'.URL.'home/userProfile/'.$_SESSION['user_id']);
         }else{
             $_SESSION["message"] =  "Successfully edited profile ";
            header('Location:'.URL.'home/userProfile/'.$_SESSION['user_id']);
            } 
         }else{
            $this->editInfo($_SESSION['user_id']);
            exit();
            }
        }else{
            $this->editInfo($_SESSION['user_id']);
            exit();
        }
    }

    
    public function denied($error){
           $loader = new Loader();
           try{
            $loader->view('denied.php',$error);
           }catch(Exception $e){
            echo 'Message'.$e->getMessage();
           }
    }   

    /*TODO  this function should later be moved to user controller
     */
    public function search(){
      
     $user = new User();

        $array_results= $user->get_user_by_firstname($_GET['search_request']);
    
       

        if(isset($array_results)){
            try{
        $this->loader->view('userProfile.php',$array_results);
        }catch(Exception $e){
            echo 'Message '.$e->getMessage();
        }
        
        }
    }
    
    public function resetPassword(){
      $loader = new Loader();
           try{
            
            $loader->view('reset-password.php');
            
           }catch(Exception $e){
            echo 'Message'.$e->getMessage();
           }  
    }
    public function sendPass(){
        $loader = new Loader();
        $loader->service("MainService.php");
        $mainService = new MainService();
        
        if(!(empty($_POST['reg_number']) && !(empty($_POST['email']))) ){
         if($mainService->sendNewPass($_POST['reg_number'],$_POST['email'])){   
          $message = "Login to your email to get a new password";
          
         }else{
           $message = "Problem in sending email, Please enter correctly the registration number
                       and the email address"; 
         }
         }else{
            $message = "Fill registration and email address first"; 
         }
         try{
            
            $loader->view('reset-password.php',$message);
            
           }catch(Exception $e){
            echo 'Message'.$e->getMessage();
           } 
    }
    
    public function siteSearch(){
        $loader = new Loader();
        try{
            $loader->service("MainService.php");
        }catch(Exception $e){
            echo 'Message'.$e->getMessage();
            
        }
        $query = $_GET['searchQuery'];
        $mainService = new MainService();
        
        $results = $mainService->search($query);
        
        try{
          $loader->view('searchResults.php',$results);  
        }
        catch(Exception $e){
            echo 'Message'.$e->getMessage();
        }
    }
    
    //for testing pages
    public function test(){
        try{
        $this->loader->view('icons-reference.html');
        }catch(Exception $e){
            echo 'Message '.$e->getMessage();
        }
    }
    

}
?>
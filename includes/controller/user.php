<?php

class UserController extends Controller{
	private $user;
	public $db;
	public $loader;

	public function __construct(){
		$this->loader = new Loader();
		$this->db = new Database();
		$this->user = new User();
	}
    
    public function index(){
        $this->loader->view('home.php');  
    }

    public function add_new_project(){
    	$users = $this->user->get_all();
    	$this->loader->view('add_project.php',$users);
    }

    public function add_new_event(){
    	$this->loader->model('event.php');
    	$event = new Event();
    	$event_categories = $event->get_categories();
    	$this->loader->view('add_event.php',$event_categories);
    }

    public function all_events(){
    	$this->loader->model('event.php');
    	$event = new Event();
    	$events = $event->get_all();
	
	$this->loader->service('MainService.php');
	$users = $this->user->get_all();
	
         $mainService = new MainService();
	 
         $data = $mainService->loadData();
	 $data['users'] = $users;
	 $data['events'] = $events;
	
	
    	$this->loader->view('all_events.php',$data);
    }

    public function all_projects(){
	$this->loader->service('MainService.php');
	
    	
	$users = $this->user->get_all();
	
         $mainService = new MainService();
	 
         $data = $mainService->loadData();
	 $project = new Project();
    	 $projects = $project->get_all();
	 $data['users'] = $users;
	 $data['projects'] = $projects;
	
      
    	$this->loader->view('all_projects.php',$data);
    }

    public function all_members(){
    	
	$this->loader->service('MainService.php');
	$users = $this->user->get_all();
	
         $mainService = new MainService();
	 
         $data = $mainService->loadData();
	 $data['users'] = $users;
	 
	
	
    	$this->loader->view('all_members.php',$data);
    }

    public function add_income(){
    	$users = $this->user->get_all();
    	$this->loader->model('finance.php');
    	$finance = new Finance();
    	$income_categories = $finance->get_income_categories();
    	$data = array(
                   'users'=>$users,
                   'income_categories'=>$income_categories
    		    );
    	$this->loader->view('add_income.php',$data);
    }

     public function add_expense(){
    	$this->loader->view('add_expense.php');
     }
     //function for sending messages
     
     public function sendMessage(){
	try{
	$this->loader->model("message.php");
	}catch(Exception $e){
		echo 'Message'.$e->getMessage();
	}
	if(isset($_POST["message"]) && $_POST["message"]!=NULL){
	$message_content = $_POST["message"];
	$receiver_id = $_POST["receiver_id"];
	
	$message = new Message($_SESSION["user_id"],$receiver_id,$message_content);
	if($message->add_message()){ 
	   echo "Message sent";
	}else{
		
	   echo "Problem in sending message";
	}
	}else{
	    echo "Problem in sending message, write something first";	
	}
	
	
     }
     
     public function messages($id=""){
	try{
	$this->loader->service('MainService.php');
	$this->loader->service('MessageService.php');
	
	}catch(Exception $e){
		echo 'Message'.$e->getMessage();
	}
	
	//loading first data from mainservice 
	$mainService = new MainService();
	$messageService = new MessageService();
	
	//loading data and at the same time loading message domain class
         $dataFromService = $mainService->loadData();
	 
	 
	$messages = (new Message())->get_all($_SESSION["user_id"]);
	
	
	//arranging messages respectively with their senders
	$arranged_messages = $messageService->arrange($messages);
	$arranged_messages = $messageService->loadConversations($arranged_messages);
	
	
	$message = new Message();
	$data  = array(
		       "messages" => $arranged_messages,
		       "clickedMessage" => $id,
		       "user" => $dataFromService['user'],
		       "posts" => $dataFromService['posts']
		       );
	$this->loader->view('messages.php',$data);
	
     }
     
     public function checkNewMessage(){
	try{
	$this->loader->model("message.php");
	}catch(Exception $e){
		echo 'Message'.$e->getMessage();
	}
	$newMessage = (new Message())->newMessage($_SESSION['user_id']);
	echo count($newMessage);
	
     }
     
     public function search(){
	$loader = new Loader();
        try{
		$loader->service("CurrentPage.php");
	}catch(Exception $e){
                echo 'Message'.$e->getMessage();
	}
	$output = '';
	if(isset($_POST['searchValue']) && isset($_POST["dataPage"])){
		$searchValue = $_POST['searchValue'];
		$page = $_POST["dataPage"];
		$user = new User();
	$results = $user->search_user($searchValue);
	
	if($results==NULL){
		$output = "No results";
	}else{
	    if($page != NULL ){}
	     if($page == "userhome" || $page == "userposts" ||
		$page == "messages" || $page == "userprofile"){
		$rp = "../../";
	     }else if($page == "user_current_projects"
		      || $page == "all_members" ||
		      $page == "user_current_events"){
		$rp = "./../";
		
	     }else{
		
		$rp = "./";
	     }
	}
	for($i = 0; $i < count($results);$i++){
		if($results[$i]['id'] != $_SESSION['user_id']){
			
           // Todo uncomment the below code and extend its ability to show bold matches in search
	//       $bold = '<strong>'.$searchValue.'</strong>';
	//	$first_name = str_replace($searchValue,$bold,$results[$i]['first_name']);
	//	$last_name =  str_replace($searchValue,$bold,$results[$i]['last_name']);
	//	$email =  str_replace($searchValue,$bold,$results[$i]['email_address']);
		
		
		$output .=
		'<li class="searchList" >
		 <a class="resultLnk" style="padding:0px;" href="/user/viewUser/'.$results[$i]['id'].'" >
	        <div class="userLink  col-lg-12">';
		
		if($results[$i]['profile_picture'] != NULL){
		   $output.= '<img class="img col-lg-2 col-md-2 col-sm-2 col-xs-2"  src="'.$rp.'pub/img/userImages/'.$results[$i]['profile_picture'].'" ';
		 }else{
		    $output.= '<img class="img col-lg-2 col-md-2 col-sm-2 col-xs-2"  src="'.$rp.'pub/img/avatars/profileImage.jpg" ';
		 }
		 $output.='
		<div class="col-lg-10">
		  '.$results[$i]['first_name'].' '.$results[$i]['last_name'].'<br><br>
		   '.$results[$i]['email_address'].'
		 
	       </div>
	       </a>
		
		</li>';
		}
	}
	}
	
	echo $output;
	
     }
     
     //action for querying post
     public function loadPost($noPost=""){
	try{
	$this->loader->model("post.php");
	}catch(Exception $e){
		echo 'Message'.$e->getMessage();
	}
	$post = new Post();
	$posts = $post->newPost();
	$output='';
	
	foreach($posts as $post){
		$output = ' "'.$post->get_content().'"';
	}
     }
     //action for writing posts
     public function post(){
	try{
	   $this->loader->model("post.php");
	   $this->loader->model("picture.php");
	   $this->loader->model("file.php");
	}catch(Exception $e){
	    echo 'Message'.$e->getMessage();
	}
	if(isset($_POST['content']) && $_POST['content'] != NULL){
	$post_content = $_POST['content'];
	$content_type="text";
	$uploadImageId = "";
	
	if(isset($_POST['uploadImageId']) && $_POST['uploadImageId'] != NULL ){
		$post_type = 'withImage';
		$uploadImageId = $_POST['uploadImageId']; 
	}
	else if(isset($_POST['uploadFileId']) && $_POST['uploadFileId'] != NULL ){
		$post_type = 'withFile';
		$uploadImageId = $_POST['uploadImageId']; 
	}else{
		$post_type = 'normal';
	}
	if($_SESSION['role'] == "member"){
           $post_type="normal";
	}
	$post = new Post($post_type,$content_type,$post_content,
			 $_SESSION['user_id'],NULL,$uploadImageId);
	if($post->add_post()){
	    if(isset($_POST['uploadImageId']) && $_POST['uploadImageId'] != NULL ){
		$picture = (new Picture())->get_picture($_POST['uploadImageId']);
		$post = $post->get_post_object();
		$picture->set_post_id($post->get_id());
		if($picture->edit_picture()){
		     echo 'success';
		     exit();
		}else{
		     echo 'failed';
		     exit();
		}
	    }
	    if(isset($_POST['uploadFileId']) && $_POST['uploadFileId'] != NULL ){
		$file = (new File())->get_file($_POST['uploadFileId']);
		$post = $post->get_post_object();
		$file->set_post_id($post->get_id());
		if($file->edit_file()){
		     echo 'success';
		     exit();
		}else{
		     echo 'failed';
		     exit();
		}
	    }
	    echo 'success';
	    exit();
	   	
	}else{
            echo 'failed';
	}
	}
     }
     
     //viewing user profile
     public function viewUser($id=""){
	 $loader = new Loader();
         
         $loader->service('MainService.php');
         $mainService = new MainService();
         $data = $mainService->loadData();
	 $data['user'] = (new User())->get_user($id);
        
        
        try{
            $loader->view('viewUser.php',$data);
        }catch(Exception $e){
            echo 'Message'.$e->getMessage();
        }
     }
     
     
     //deleting message
     public function deleteMessage($messageId=""){
	
	if($messageId!=NULL && is_numeric($messageId)){
		$messageId = (int)$messageId;
	    if($messageId > 0){
		try{
		$this->loader->model("message.php");
		}catch(Exception $e){
			echo 'Message'.$e->getMessage();
		}
		$message = new Message();
		$message = $message->get_by_id($messageId);
		if($message->delete($messageId)){
		    echo "success";
		
		}else{
			echo "failed";
		}
	    }else{
		echo "failed not number";
		}	
	}else{
		echo "failed";
	}
	
     }
     
     public function uploadPostImage(){
	$loader = new Loader();
	try{
	$loader->model("picture.php");
	}catch(Exception $e){
		echo 'Message'.$e->getMessage();
	}
	$picture  = new Picture();
	$imageService = new ImageService();
	
	$uploaddir = './pub/img/postImages/';
	
	
	if(isset($_FILES['file']) && $_FILES['file'] != NULL){
		
	   $filename = substr($_FILES['file']['name'],0,strrpos($_FILES['file']['name'],'.'));
	   $filename .=','.date("Ymd,H:i:s");
	   $filename .= '.'.pathinfo($_FILES['file']['name'])['extension'];
           $file = $uploaddir.$filename;
	   
	   $picture->set_url($file);

         if ($imageService->saveImage($file,"post")) {
		
		if($picture->add_picture()){
		$picture = $picture->get_by_url($picture->get_url());
		$value = "success".$picture->get_id();
		}else{
			$value = Picture::$picture_error;
		}
         } 
         else {
         $value = "error";
         }


        echo $value; 
     }
     else{
	echo 'Upload first';
      }
    }
    //file uploading
     public function uploadPostFile(){
	$loader = new Loader();
	try{
	$loader->model("file.php");
	}catch(Exception $e){
		echo 'Message'.$e->getMessage();
	}
	$file  = new File();
	$imageService = new ImageService();
	
	$uploaddir = './pub/files/postFiles/';
	
	
	if(isset($_FILES['fileAttached']) && $_FILES['fileAttached'] != NULL){
		
	   $filename = substr($_FILES['fileAttached']['name'],0,strrpos($_FILES['fileAttached']['name'],'.'));
	   $filename .=','.date("Ymd,H:i:s");
	   $filename .= '.'.pathinfo($_FILES['fileAttached']['name'])['extension'];
           $fileup = $uploaddir.$filename;
	   
	   $file->set_url($fileup);

         if ($imageService->saveFile($fileup,"post")) {
		
		if($file->add_file()){
		$file = $file->get_by_url($file->get_url());
		$value = "success".$file->get_id();
		}else{
			$value = "file error";
		}
         } 
         else {
         $value = "error";
         }


        echo $value; 
     }
     else{
	echo 'Upload first';
      }
    }
    
    
    
    
    
    //for commenting
    public function postComment(){
	try{
	$this->loader->model("comment.php");
	}catch(Exception $e){
		echo 'Message'.$e->getMessage();
	}
	if(isset($_POST["comment"]) && $_POST["comment"]!=NULL){
	$comment_content = $_POST["comment"];
	$post_id = $_POST["post_id"];
	
	$comment = new Comment($_SESSION["user_id"],$post_id,$comment_content);
	if($comment->add_comment()){ 
	   echo "Comment posted";
	}else{
		
	   echo "Problem in posting comment".Comment::$comment_error;
	}
	}else{
	    echo "Problem in posting comment, write something first";	
	}
    }
    
    public function checkNewComments(){
	try{
	$this->loader->model("comment.php");
	}catch(Exception $e){
		echo 'Message'.$e->getMessage();
	}
	$newComments = (new Comment())->newComments();

	
	if($newComments != NULL){
	    $this->loader->template("user/post/comment_content.php",$newComments);
	}
	
	
     }
     
     public function posts($user_id=""){
	try{
	$this->loader->service("MainService.php");
	}catch(Exception $e){
		echo 'Message'.$e->getMessage();
	}
	$mainService = new MainService();
	$data = $mainService->loadData();
	
	$post = new Post();
	$post = $post->get_post_by_user_id($user_id);
	
	
	$data["posts"] = $post;
	
	echo Post::$post_error;
	$this->loader->view("posts.php",$data);
	
	
     }
     //for checking post
     public function checkNewPost(){
	try{
	$this->loader->model("post.php");
	}catch(Exception $e){
		echo 'Message'.$e->getMessage();
	}
	$newPosts = (new Post())->newPosts();
	
	if(isset($newPosts) && $newPosts!= NULL){
	    $this->loader->model("picture.php");
	    $this->loader->model("comment.php");
	    $this->loader->template("user/post/post_content.php",$newPosts);
	}else{
		
	}
	
	
     }
     //for checking post
     public function morePosts(){
	try{
	$this->loader->model("post.php");
	}catch(Exception $e){
		echo 'Message'.$e->getMessage();
	}
	$loadPosts = $_SESSION['loaded_posts'] + 4;
	$_SESSION['loaded_posts'] += 4;
	$morePosts = (new Post())->get_posts($loadPosts);
	
	if(isset($morePosts) && $morePosts!= NULL){
	    $this->loader->model("picture.php");
	    $this->loader->model("comment.php");
	    $this->loader->template("user/post/post_content.php",$morePosts);
	}
	
	
     }
     
     public function deletePost($postId = ""){
	
	if($postId!=NULL && is_numeric($postId)){
		$postId = (int)$postId;
	    if($postId > 0){
		try{
		$this->loader->model("post.php");
		}catch(Exception $e){
			echo 'Message'.$e->getMessage();
		}
		$post = new Post();
		if($post->delete($postId)){
		    echo "success";
		
		}else{
			echo "failed";
		}
	    }else{
		echo "failed not number";
		}	
	}else{
		echo "failed";
	}
	
     }
     
     
     
}


?>
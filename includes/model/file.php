<?php

class File {
    private $id;
    private $url;
    private $post_id;
    private $user_id;
    private $date_created;
    
    public static $file_error;
    
    
    public function __construct($url="",$post_id="",$user_id=""){
        $this->url = $url;
        $this->post_id = $post_id;
        $this->user_id = $user_id;
        
        $this->date_created = date("Y-m-d H:i:s");
        
    }
    
    public function set_url($url=""){
        $this->url = $url;
    }
    public function set_post_id($post_id=""){
        $this->post_id = $post_id;
    }
    public function set_user_id($user_id=""){
        $this->user_id = $user_id;
    }
    
    
    public function get_id(){
        return $this->id;
    }
    public function get_url(){
        return $this->url;
    }
    public function get_post_id(){
        return $this->post_id;
    }
    public function get_user_id(){
        return $this->user_id;
    }
    
    
    
    public function add_file(){
        $sql = "INSERT INTO file (url,post_id,user_id,date_created) ";
            $sql.= "VALUES('".$this->url."','".$this->post_id."','".$this->user_id."','".$this->date_created."')";
            
            global $db;

            if($db->db_query($sql)){
              return $db->db_last_insert_id();
            }else{
                 $this::$file_error = $db->last_query;  
            }
    }
    public function edit_file(){
            $sql = "UPDATE file SET url = '".$this->url."',post_id = '".$this->post_id."',";
            $sql .=" user_id = '".$this->user_id."' WHERE id ='".$this->id."'";
            global $db;
            if($db->db_query($sql)){
                   return $db->db_affected_rows();
            }else{
                   $this::$file_error = $db->last_query;
                   return false;
            }   
    }
    
    public function get_file($id=""){
          $sql = "SELECT * FROM file WHERE id = '".$id."'";
          global $db;
          if($results = $db->db_query($sql)){
             $result_files = $db->db_fetch_array($results);
             $files_objects = $this->arrayToObject($result_files);
                return $files_objects;
          }else{
             $this::$file_error = $db->last_query;  
          }
    }
    public function get_by_url($url=""){
        $sql = "SELECT * FROM file WHERE url = '".$url."' LIMIT 1";
          global $db;
          if($results = $db->db_query($sql)){
             $result_files = $db->db_fetch_array($results);
             $files_objects = $this->arrayToObject($result_files);
                return $files_objects;
          }else{
             $this::$file_error = $db->last_query;  
          }
    }
    
    private function arrayToObject($files_array){
        
        if(count($files_array) != 1){
            $file_objects = array();
        for($i = 0; $i <count($files_array) ; $i++) {
           $file = $files_array[$i];
           
           $file = new File($file['url'],$file['post_id'],$file['user_id']);
           $file->id =  $files_array[$i]['id'];
           $file->date_created = $files_array[$i]['date_created'];
           $file_objects[$i] = $file;
        }
        return $file_objects;
        }else{
           $file = $files_array[0];
           
           $file = new File($file['url'],$file['post_id'],$file['user_id']);
           $file->id =  $files_array[0]['id'];
           $file->date_created = $files_array[0]['date_created'];
           return $file;
        }
    }
}




?>
<?php

    class Message {
        
        private $message_id;
        private $user_sender_id;
        private $user_receiver_id;
        private $message;
        private $seen;
        private $deleted;
        private $date_created;
        
        public static $message_error;
        public static $error;
        
        public function __construct($user_sender_id="",$user_receiver_id="",
                                   $message=""){
            
           
            $this->user_sender_id = $user_sender_id;
            $this->user_receiver_id = $user_receiver_id;
            $this->message = $message;
            $this->seen = false;
            $this->deleted = false;
            $this->date_created = date("Y-m-d H:i:s");
            
            
        }
        public function get_user_sender_id(){
            return $this->user_sender_id;
        }
        public function get_user_receiver_id(){
            return $this->user_receiver_id;
        }
        public function get_id(){
            return $this->message_id;
        }
        public function get_message(){
            return $this->message;
        }
        public function get_deleted(){
            return $this->deleted;
        }
        public function get_date(){
            return $this->date_created;
        }
        public function get_userObj(){
            $user = (new User())->get_user($this->user_sender_id);
            return $user;
        }
        
        public function get_seen(){
            return $this->seen;
        }
        
        
        public function setSeen($seen=""){
            $this->seen = $seen;
            $this->updateSeen($this->message_id);
        }
        public function setAllSeen($user_id){
            $this->updateSeen($user_id);
        }
        
        public function add_message(){
            $sql = "INSERT INTO messages (date_created,user_sender_id,user_receiver_id,message)";
            $sql.= "VALUES('".$this->date_created."','".$this->user_sender_id."','".$this->user_receiver_id."','".$this->message."' ";
            $sql.= ")";
            
            global $db;

            if($db->db_query($sql)){
              if($this->add_to_message_track($db->db_last_insert_id())){
                 return $db->db_last_insert_id();
              }
               
            }else{
                 $this::$message_error = $db->last_query;  
            }
        }
        
        
        private function add_to_message_track($msg_id=""){
            if($msg_id != NULL){
             $sql = "INSERT INTO user_message (user_id,msg_id,seen,deleted,date_created)";
             $sql.= "VALUES('".$this->user_receiver_id."','".$msg_id."',";
             $sql.= "'0','0','".$this->date_created."')";
             
              global $db;
              if($db->db_query($sql)){
                return true;
              }else{
                $this::$message_error = $db->last_query;
                return false;
              }
              
              
            }else{
                $this::$message_error = "message id is null";
                return false;
            }
            
        }
        
        public function newMessage($user_receiver_id=""){
            $sql = "SELECT DISTINCT (ms.message_id),ms.*,um.seen,um.deleted FROM messages ms ";
            $sql .= "JOIN user_message um ON um.msg_id = ms.message_id ";
            $sql .= "WHERE um.user_id = '".$user_receiver_id."'";
            global $db;
            if($results = $db->db_query($sql)){
                $result_messages = $db->db_fetch_array($results);
                $messages_objects = $this->arrayToObject($result_messages);
                
            
             
             $message_arr = array();
             
             //finding new messages that user haven't see
             if($messages_objects != NULL){
                if(is_object($messages_objects)){
                    $nsql = "SELECT * FROM user_message WHERE ( msg_id = '".$messages_objects->get_id()."' ";
                $nsql .=" AND user_id = '".$_SESSION['user_id']."') AND (seen = '0' AND deleted = '0')  LIMIT 1";
                if($result_for_message = $db->db_query($nsql)){
                    $result_for_message = $db->db_fetch_array($result_for_message);
                     
                    if(count($result_for_message) == 1){
                        array_push($message_arr,$messages_objects);
                       
                    }else{
                    }
                   
                    
                }else{
                    $this::$message_error = $db->last_query;
                }
                    
                    
                    
                    
                }else{
                  foreach($messages_objects as $message){
                if($message != NULL && is_object($message)){
                $nsql = "SELECT * FROM user_message WHERE ( msg_id = '".$message->get_id()."' ";
                $nsql .=" AND user_id = '".$_SESSION['user_id']."') AND (seen = '0' AND deleted = '0')  LIMIT 1";
                if($result_for_message = $db->db_query($nsql)){
                    $result_for_message = $db->db_fetch_array($result_for_message);
                     
                    if(count($result_for_message) == 1){
                        array_push($message_arr,$message);
                       
                    }else{
                        continue;  
                         
                    }
                   
                    
                }else{
                    $this::$message_error = $db->last_query;
                }
                }else{
                    $this::$message_error = "Some value is null";
                }
             }
                }
             }
            return $message_arr;
            
            }else{
                $this::$message_error = $db->last_query;  
            }
        }
        
        public function get_user_sender_name(){
           
            $user = (new User())->get_user($this->user_sender_id);
            $userName = $user->get_fullName();
            return $userName;
        }
        public function get_all($id=""){
          $sql = "SELECT  DISTINCT (ms.message_id),ms.*,um.seen, um.deleted FROM messages ms ";
          $sql .="JOIN user_message um ON um.msg_id = ms.message_id ";
          $sql .="WHERE (user_receiver_id = '".$id."' or user_sender_id = '".$id."') ";
          $sql.=  "AND um.deleted = '0' ORDER BY ms.date_created DESC";
          global $db;
          if($results = $db->db_query($sql)){
             $result_messages = $db->db_fetch_array($results);
             $messages_objects = $this->arrayToObject($result_messages);
                return $messages_objects;
          }else{
             $this::$message_error = $db->last_query;  
          }
        }
        
         public function get_by_id($message_id = ""){
          if(!empty($message_id)){
             $sql = "SELECT * FROM messages WHERE message_id = ".$message_id." LIMIT 1";
             global $db;
             if($message = $db->db_query($sql)){
                 $array =  $db->db_first_row($message);
                 
                 $messageObject = $this->arrayToObject($message);
		 
		  return $messageObject;
             } else{
                 $this::$message_error = $db->last_query;
             }
          }
   	  }
        
        public function delete($message_id=""){
            $sql = "UPDATE user_message SET deleted = '1' ";
            $sql.= "WHERE user_id = '".$this->user_receiver_id."' ";
            $sql.= "AND msg_id = '".$message_id."' ";
            
            global $db;
            
            if($db->db_query($sql)){
              return true;
            }else{
                 $this::$message_error = $db->last_query;
                 return false;
            }
        }
        
        private function updateSeen($user_id){
            $sql = "UPDATE user_message SET seen = '1' ";
            $sql.= "WHERE user_id = '".$user_id."' ";
            
            global $db;
            
            if($db->db_query($sql)){
              return $db->db_last_insert_id();
            
            }else{
                 $this::$message_error = $db->last_query;  
            }
        }
        private function updateSeenWithId($message_id){
            $sql = "UPDATE user_message SET seen ='1'";
            $sql.= "WHERE user_id = '".$this->user_receiver_id."' ";
            $sql.= "AND msg_id = '".$message_id."' ";
            
            global $db;
            
            if($db->db_query($sql)){
              return $db->db_last_insert_id();
            }else{
                 $this::$message_error = $db->last_query;  
            }
        }
        
        private function arrayToObject($messages_array){
            if(count($messages_array) > 0){
            if(count($messages_array) != 1){
            $message_objects = array();
            for($i = 0; $i <count($messages_array) ; $i++) {
               $message = $messages_array[$i];
               
               $message = new Message($message['user_sender_id'],$message['user_receiver_id'],$message['message']);
               $message->message_id =  $messages_array[$i]['message_id'];
               $message->seen =  $messages_array[$i]['seen'];
               $message->deleted =  $messages_array[$i]['deleted'];
               $message->date_created = $messages_array[$i]['date_created'];
               $message_objects[$i] = $message;
            }
            
            return $message_objects;
            }else{
               $message= $messages_array[0];
	   
               $message = new Message($message['user_sender_id'],$message['user_receiver_id'],$message['message']);
               $message->message_id =  $messages_array[0]['message_id'];
               $message->seen =  $messages_array[0]['seen'];
               $message->deleted =  $messages_array[0]['deleted'];
               $message->date_created = $messages_array[0]['date_created'];
               
               return $message;  
            }
            }else{
                return NULL;
            }
        }
        
        
        
        
      
    }
    

?>
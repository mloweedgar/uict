<?php

    class Post {
        
        private $post_id;
        private $post_type;
        private $content_type;
        private $content;
        private $attachment_type;
        private $attachment;
        private $picture_id;
        private $file_id;
        private $user_id;
        private $seen;
        private $date_created;
        
        public static $post_error;
        
        public function __construct($post_type="",$content_type="",
                                   $content="",$user_id="",
                                   $attachment="",$picture_id="",$attachment_type="",$file_id=""
                                   ){
            
           
            $this->post_type = $post_type;
            $this->content_type = $content_type;
            $this->content = $content;
            $this->attachment_type = $attachment_type;
            $this->attachment = $attachment;
            $this->picture_id = $picture_id;
            $this->user_id = $user_id;
            $this->seen = false;
            $this->date_created = date("Y-m-d H:i:s");
            
            
        }
        public function add_post(){
            $sql = "INSERT INTO post (post_type,content_type,content,attachment_type,";
            $sql.= "attachment,picture_id,user_id,seen,date_created) ";
            $sql.= "VALUES('".$this->post_type."','".$this->content_type."','".$this->content."','".$this->attachment_type."',";
            $sql.= "'".$this->attachment."','".$this->picture_id."','".$this->user_id."','".$this->seen."','".$this->date_created."')";
            
            global $db;

            if($db->db_query($sql)){
              return $db->db_last_insert_id();
            }else{
                 $this::$post_error = $db->last_query;  
            }
        }
        public function get_all(){
          $sql = "SELECT * FROM post WHERE deleted = '0'";
          $sql.=  "ORDER BY date_created DESC ";
          global $db;
          if($results = $db->db_query($sql)){
             $result_posts = $db->db_fetch_array($results);
             $post_objects = $this->arrayToObject($result_posts);
                return $post_objects;
          }else{
             $this::$post_error = $db->last_query;  
          }
        }
        public function get_posts($limit=""){
            $sql = "SELECT * FROM post  WHERE deleted = '0' ";
            $sql.=  "ORDER BY date_created DESC LIMIT ".$limit."";
          global $db;
          if($results = $db->db_query($sql)){
            
             $result_posts = $db->db_fetch_array($results);
             $post_objects = $this->arrayToObject($result_posts);
             
                return $post_objects;
          }else{
             $this::$post_error = $db->last_query;  
          }
        }
        
        public function get_post_object(){
            $sql = "SELECT * FROM post WHERE date_created = '".$this->date_created."' AND deleted = '0' LIMIT 1";
          global $db;
          if($results = $db->db_query($sql)){
             $result_posts = $db->db_fetch_array($results);
             $post_objects = $this->arrayToObject($result_posts);
                return $post_objects;
          }else{
             $this::$post_error = $db->last_query;  
          }
        }
        
        //getting unseen posts
        public function newPosts(){
            $sql = "SELECT * FROM post WHERE deleted = '0'";
            $sql .= "";
            //$sql .= "WHERE up.user_id <> '".$_SESSION['user_id']."' AND p.id <> up.post_id";
            //$sql .= "ORDER BY p.date_created DESC";
            global $db;
            if($results = $db->db_query($sql)){
             $result_posts = $db->db_fetch_array($results);
             $post_objects = $this->arrayToObject($result_posts);
             
             $postarr = array();
             
             //finding new posts that user haven't see
              if($post_objects != NULL){
              if(is_object($post_objects)){
                 $nsql = "SELECT * FROM user_post WHERE  (post_id = '".$post_objects->get_id()."' ";
               
                // $nsql = "SELECT * FROM user_post WHERE  (post_id = '".$post->get_id()."' ";   
                
                $nsql .=" AND user_id = '".$_SESSION['user_id']."')  LIMIT 1";
                if($result_for_post = $db->db_query($nsql)){
                    $result_for_post = $db->db_fetch_array($result_for_post);
                    
                    if(count($result_for_post) > 0){
                    
                    }else{
                        array_push($postarr,$post_objects); 
                    }
                   
                    
                }else{
                    $this::$post_error = $db->last_query;
                }
                }
                else{
                foreach($post_objects as $post){
               
                $nsql = "SELECT * FROM user_post WHERE  (post_id = '".$post['id']."' ";
               
                // $nsql = "SELECT * FROM user_post WHERE  (post_id = '".$post->get_id()."' ";   
                
                $nsql .=" AND user_id = '".$_SESSION['user_id']."')  LIMIT 1";
                if($result_for_post = $db->db_query($nsql)){
                    $result_for_post = $db->db_fetch_array($result_for_post);
                    
                    if(count($result_for_post) > 0){
                       continue; 
                    }else{
                        array_push($postarr,$post); 
                    }
                   
                    
                }else{
                    $this::$post_error = $db->last_query;
                }
              }
              }
              }
                return $postarr;
          }else{
             $this::$post_error = $db->last_query;  
          }
        }
        
        
        public function get_post_by_user_id($user_id=""){
            if($user_id != NULL){
                global $db;
                
               $user_id = $db->db_escape_values($user_id); 
            $sql = "SELECT * FROM post WHERE user_id = '".$user_id."'  ";
            $sql.=" AND deleted = '0'";
            if($results = $db->db_query($sql)){
            
             $result_posts = $db->db_fetch_array($results);
             $post_objects = $this->arrayToObject($result_posts);
             
                return $post_objects;
          }else{
             $this::$post_error = $db->last_query;  
          }
            }
        }
        
        public function delete($postId=""){
            if($postId != NULL){
                global $db;
                $postId = $db->db_escape_values($postId);
            $sql = "UPDATE post SET deleted = '1' ";
            $sql.= "WHERE id = '".$postId."' ";
            
            
            
            if($db->db_query($sql)){
              return true;
            }else{
                 $this::$post_error = $db->last_query;
                 return false;
            }
            }
        }
        
        
        
        
        
        
        //function to return object when given an array
        
        private function arrayToObject($posts_array){
            if(count($posts_array) > 0){
            if(count($posts_array) != 1){
            $post_objects = array();
            for($i = 0; $i <count($posts_array) ; $i++) {
               $post = $posts_array[$i];
               
               $post = new Post($post['post_type'],$post['content_type'],
                                $post['content'],$post['user_id'],
                                $post['attachment'],$post['picture_id'],$post['attachment_type'],
                                $post['file_id']);
               
               // using $posts_array[$i] because $post is has changed state from array to object
               
               $post->post_id =  $posts_array[$i]['id'];
               $post->date_created = $posts_array[$i]['date_created'];
               $post->seen = $posts_array[$i]['seen'];
               $post_objects[$i] = $post;
            }  
               return $post_objects;
            
            }else{
                $post = $posts_array[0];
               
               $post = new Post($post['post_type'],$post['content_type'],
                                $post['content'],$post['user_id'],
                                $post['attachment'],$post['picture_id'],$post['attachment_type'],
                                $post['file_id']);
               
               // using $posts_array[$i] because $post is has changed state from array to object
               
               $post->post_id =  $posts_array[0]['id'];
               $post->date_created = $posts_array[0]['date_created'];
               $post->seen = $posts_array[0]['seen'];
               return $post;
            }
            }else{
                return NULL;
            }
            
            
        }
        public function updateSeen($id){
            if(isset($id) && $id != NULL){
            $sql = "INSERT INTO user_post (user_id,post_id,seen,date_created)";
            $sql .="VALUES ('".$_SESSION["user_id"]."','".$id."','1','".date("Y-m-d H:i:s")."')";
            
            global $db;
            if($results = $db->db_query($sql)){
                return true;
            }else{
                $this::$post_error = $db->last_query;
              }
            }
            
        }
        public function updatePictureId(){
            $sql = "UPDATE post SET picture_id = '".$this->picture_id."' WHERE id = '".$this->post_id."'";
            global $db;
            if($results = $db->db_query($sql)){
                return true;
            }else{
                return false;
            }
            
        }
        
        
        
        public function get_id(){
            return $this->post_id;
        }
        
        public function get_user_id(){
            return $this->user_id;
        }
        public function get_content(){
            return $this->content;
        }
        public function get_post_type(){
            return $this->post_type;
        }
        public function get_picture_id(){
            return $this->picture_id;
        }
        public function get_file_id(){
            return $this->file_id;
        }
        public function get_date_created(){
            return $this->date_created;
        }
       
        public function set_file_id($file_id=""){
            $this->file_id = $file_id;
        }
        
        public function set_picture_id($picture_id=""){
            $this->picture_id = $picture_id;
        }
    }
        
?>
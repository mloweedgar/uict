<?php
   /* User object should:-
    * 1. Add user (Full registration)
    * 2. Edit user 
    * 3. Delete user
    * 4. Activate user
    * 5. Deactivate user
    * 6. Retrieve all users
    * 7. Retrieve single user
    */



   class User{
   	  private $id;
   	  private $first_name;
   	  private $last_name;
   	  private $reg_number;
   	  private $grad_year;
   	  private $program_id;
   	  private $year_of_study;
   	  private $active_status;
   	  private $gender;
          private $mailing_address;
          private $email_address;
   	  private $phone_number;
   	  private $role;
   	  private $status;
	  private $skills;
	  private $hobbies;
	  
   	  private $password;
	  private $profile_picture;
	  private $registered_date;
	  
	  public static $user_error;
	  
	  public function __construct($id="",$first_name="",$last_name="",$reg_number="",$grad_year="",$program_id="",
				      $year_of_study="",$active_status="",$gender="",$mailing_address="",$email_address="",
				      $phone_number="",$role="",$status="",$profile_picture="",$skills="",$hobbies=""){
					  
	      $this->id = $id;
	      $this->first_name = $first_name;
	      $this->last_name = $last_name;
	      $this->reg_number = $reg_number;
	      $this->grad_year = $grad_year;
	      $this->program_id = $program_id;
	      $this->year_of_study = $year_of_study;
	      $this->active_status = $active_status;
	      $this->gender = $gender;
              $this->mailing_address = $mailing_address;
	      $this->email_address = $email_address;
	      $this->phone_number = $phone_number;
	      $this->role = $role;
	      $this->status = $status;
	      $this->profile_picture = $profile_picture;
	      $this->skills = $skills;
	      $this->hobbies = $hobbies;
	      
	      date_default_timezone_set('Africa/Dar_es_Salaam');
	      $this->registered_date = date("Y-m-d H:i:s");
	      
	      
	  }

   	  public function set_password($pass = ""){
         if(!empty($pass)){
            $this->password = $pass;
         }
   	  }

      public function authenticate($reg_number="",$pass=""){
          if(!(empty($reg_number) && empty($pass))){
              $sql = "SELECT * FROM users WHERE reg_number = '".$reg_number."' AND password = '".sha1($pass)."' LIMIT 1"; 
              global $db;
              if($user = $db->db_query($sql)){
                   $user = $db->db_first_row($user);
		   
		   return $this->get_user($user[0]);
	      
              }else{
		     $this::$user_error = $db->last_query;
		     
		 }
          }
      }

   	  public function add_user(){
   	  	 $sql = "INSERT INTO users (first_name,last_name,reg_number,grad_year,program_id,year_of_study,";
   	  	 $sql .= "gender,mailing_address,email_address,phone_number,role,status,password,profile_picture,registered_date,skills,hobbies)";
		 $sql.= " VALUES('".$this->first_name."',";
   	  	 $sql .= "'".$this->last_name."','".$this->reg_number."','".$this->grad_year."','".$this->program_id."',";
   	  	 $sql .= "'".$this->year_of_study."','".$this->gender."','".$this->mailing_address."','".$this->email_address."','".$this->phone_number."',";
   	  	 $sql .= "'".$this->role."','".$this->status."','".sha1($this->password)."','".$this->profile_picture."','".$this->registered_date."',";
		 $sql .="'".$this->skills."','".$this->hobbies."')";
                 global $db;


                if($db->db_query($sql)){
                  return $db->db_last_insert_id();
                }else{
		     $this::$user_error = $db->last_query;  
		 }
       }

   	  public function edit_user($user_id = "",$editPassword=""){
	        if(isset($editPassword) && $editPassword != NULL){
   	  	 if(!empty($user_id)){
	   	  	 $sql = "UPDATE users SET first_name = '".$this->first_name."',last_name = '".$this->last_name."',";
	   	  	 $sql .=" reg_number = '".$this->reg_number."',grad_year = '".$this->grad_year."',program_id = '".$this->program_id."',";
	   	  	 $sql .= "year_of_study = '".$this->year_of_study."',gender = '".$this->gender."',mailing_address='".$this->mailing_address."',email_address = '".$this->email_address."',";
	   	  	 $sql .= "phone_number = '".$this->phone_number."',role = '".$this->role."',status = '".$this->status."',
			          password = '".sha1($editPassword)."', profile_picture = '".$this->profile_picture."', skills='".$this->skills."',hobbies='".$this->hobbies."' WHERE id ='".$user_id."'";
	  	 	 global $db;
	  	 	 if($db->db_query($sql)){
	  	 		return $db->db_affected_rows();
	  	 	 }else{
                                $this::$user_error = $db->last_query;
                         }
  	 	}
		return false;
		}else{
		     if(!empty($user_id)){
			    
	   	  	 $sql = "UPDATE users SET first_name = '".$this->first_name."',last_name = '".$this->last_name."',";
	   	  	 $sql .=" reg_number = '".$this->reg_number."',grad_year = '".$this->grad_year."',program_id = '".$this->program_id."',";
	   	  	 $sql .= "year_of_study = '".$this->year_of_study."',gender = '".$this->gender."',mailing_address='".$this->mailing_address."',email_address = '".$this->email_address."',";
	   	  	 $sql .= "phone_number = '".$this->phone_number."',role = '".$this->role."',status = '".$this->status."',
			          profile_picture = '".$this->profile_picture."', skills='".$this->skills."',hobbies='".$this->hobbies."' WHERE id ='".$user_id."'";
	  	 	 global $db;
	  	 	 if($db->db_query($sql)){
	  	 		return $db->db_affected_rows();
	  	 	 }else{
                                $this::$user_error = $db->last_query;
				
                         }
  	 	}
		return false;
		}
   	  }
	  public function updatePass($pass=""){
	      if($pass != NULL){
		     $sql = "UPDATE users SET password = '".sha1($this->password)."'
		             WHERE id = '".$this->id."'";
			     global $db;
	  	 	 if($db->db_query($sql)){
	  	 		return $db->db_affected_rows();
	  	 	 }else{
                                $this::$user_error = $db->last_query;
                         }
	      }
	      
	  }

   	  public function delete_user($user_id = ""){
         if(!empty($user_id)){
            $sql = "DELETE FROM users WHERE id = ".$user_id;
            global $db;
            if($db->db_query($sql)){
               return $db->db_affected_rows();
            }
         }
   	  }	

   	  public function get_all(){
   	  	  $sql = "SELECT users.id,users.first_name,users.last_name,users.email_address,users.phone_number,users.reg_number,";
          $sql .= "users.active_status,users.grad_year,users.gender,users.mailing_address,users.role,users.status,users.profile_picture,";
          $sql .= "users.year_of_study,users.program_id,users.registered_date,users.skills,users.hobbies FROM users JOIN programs ON ";
          $sql .= "users.program_id = programs.id ORDER BY users.id ASC";
   	  	  global $db;
          if($results = $db->db_query($sql)){
              $result_users = $db->db_fetch_array($results);
              return $result_users;
          }else{
              $this::$user_error = $db->last_query;
	      return NULL;
            }
   	  }

   	  public function get_user($user_id = ""){
          if(!empty($user_id)){
             $sql = "SELECT * FROM users WHERE id = ".$user_id." LIMIT 1";
             global $db;
             if($user = $db->db_query($sql)){
                 $array =  $db->db_first_row($user);
		 
		 $returnedUser = new User($array['id'],$array['first_name'],$array['last_name'],$array['reg_number'],
			      $array['grad_year'],$array['program_id'],$array['year_of_study'],$array['active_status'],
			      $array['gender'],$array['mailing_address'],$array['email_address'],$array['phone_number'],
			      $array['role'],$array['status'],$array['profile_picture'],
	                      $array['skills'],$array['hobbies']); 
	          
		 
		  
		  return $returnedUser;
             }
          }
   	  }
      public function get_user_by_reg_number($reg_number=""){
        if(!empty($reg_number)){
             $sql = "SELECT * FROM users WHERE reg_number = '".$reg_number."' LIMIT 1";
             global $db;
             if($user = $db->db_query($sql)){
                 $array =  $db->db_first_row($user);

                 $this::$user_error="executed in get by user reg ".$user[0];

                 $returnedUser = new User($array['id'],$array['first_name'],$array['last_name'],$array['reg_number'],
            $array['grad_year'],$array['program_id'],$array['year_of_study'],$array['active_status'],
            $array['gender'],$array['email_address'],$array['phone_number'],$array['role'],$array['status'],$array['profile_picture'],
	    $array['skills'],$array['hobbies']); 

            return $returnedUser;
	          }else{
              $this::$user_error = $db->last_query;
         
       }

       }
  
       }
       
        public function get_user_by_email($email=""){
         if(!empty($email)){
             $sql = "SELECT * FROM users WHERE email_address = '".$email."' LIMIT 1";
             global $db;
             if($user = $db->db_query($sql)){
                 $array =  $db->db_first_row($user);

                 $this::$user_error="executed in get by user email ".$user[0];

                 $returnedUser = new User($array['id'],$array['first_name'],$array['last_name'],$array['reg_number'],
            $array['grad_year'],$array['program_id'],$array['year_of_study'],$array['active_status'],
            $array['gender'],$array['email_address'],$array['phone_number'],$array['role'],$array['status'],$array['profile_picture'],
	    $array['skills'],$array['hobbies']); 

            return $returnedUser;
            }
            else{
              $this::$user_error = $db->last_query;
	      return NULL;
            }
          }
	 }
       
       
       
      public function get_user_by_firstname($user_request){
        if(!empty($user_request)){
          $sql = "SELECT id,first_name,last_name FROM users WHERE first_name LIKE '%".$user_request."%' ";
         
          global $db;
          if($user = $db->db_query($sql)){
            $array = $db->db_fetch_array($user);
            return $array;
          }

        }else{
          $this::$user_error = $db->last_query;
        }
      }

      public function get_program($program_id){
            $program = Program::get_program($program_id);
       
              return $program;
      }
      public function get_latest_users($max_number=""){
       if(!empty($max_number)){
	      $sql = "SELECT * FROM users ORDER BY registered_date DESC LIMIT ".$max_number." ";
       }else{
	      $sql = "SELECT * FROM users LIMIT 1";
       }
        global $db;
          if($results = $db->db_query($sql)){
              $result_users = $db->db_fetch_array($results);
              return $result_users;
          }else{
              $this::$user_error = $db->last_query;
	      return NULL;
            }
       
      }
      public function get_fullName(){
       return $this->first_name.' '.$this->last_name;
      }
      
      public function search_user($searchValue=""){
       
       $sql = "SELECT id,first_name,last_name,email_address,phone_number,profile_picture";
       $sql.= " FROM users WHERE first_name like '%".$searchValue."%'";
       $sql.= " or last_name like '%".$searchValue."%' or email_address like '%".$searchValue."%'";
       $sql.= " or phone_number like '%".$searchValue."%' LIMIT 5;";
       
       global $db;
       if($results = $db->db_query($sql)){
              $result_users = $db->db_fetch_array($results);
              return $result_users;
          }else{
              $this::$user_error = $db->last_query;
	      return NULL;
            }
       
       
      }
      
      
      //get method
      public function get_id(){
       return $this->id;
      }
      
      public function get_first_name(){
       return $this->first_name;
      }
      public function get_last_name(){
       return $this->last_name;
      }
      public function get_reg_number(){
       return $this->reg_number;
      }
      public function get_program_id(){
       return $this->program_id;
      }
      public function get_year_of_study(){
       return $this->year_of_study;
      }
      public function get_gender(){
       return $this->gender;
      }
      
      public function get_mailing_address(){
       return $this->mailing_address;
      }
      public function get_email(){
       return $this->email_address;
      }
      public function get_phonenumber(){
       return $this->phone_number;
      }
      public function get_skills(){
       return $this->skills;
      }
      public function get_hobbies(){
       return $this->hobbies;
      }
      public function get_status(){
       return $this->status;
      }
      public function get_role(){
       return $this->role;
      }
      public function get_grad_year(){
       return $this->grad_year;
      }
      public function get_profile_picture(){
       return $this->profile_picture;
      }
      public function get_password(){
       return $this->password;
      }
      
      //loading user program
      //TODO remove this function replace it with eager or lazy
      // functionality
      
      public function get_programObj(){
       $loader = new Loader();
       try{
       $loader->model("program.php");
      
      }catch(Exception $e){
       echo 'Message'.$e->getMessage();
      }
       $program = Program::get_program($this->program_id);
       return $program;
      }      
      //set methods
      public function set_id($id){
       $this->id = $id;
      }
      public function set_first_name($firstName=""){
       $this->first_name = $firstName;
      }
      public function set_last_name($lastName=""){
       $this->last_name = $lastName;
      }
      public function set_reg_number($regNumber=""){
       $this->reg_number = $regNumber;
      }
      public function set_program_id($programId=""){
       $this->program_id = $programId;
      }
      public function set_year_of_study($yearOfStudy=""){
       $this->year_of_study = $yearOfStudy;
      }
      public function set_gender($genderFromUser=""){
       $this->gender = $genderFromUser;
      }
      public function set_status($statusFromUser=""){
       $this->status = $statusFromUser;
      }
      public function set_mailing_address($mailingAdd=""){
       $this->mailing_address = $mailingAdd;
      }
      public function set_email_address($emailAdd=""){
       $this->email_address = $emailAdd;
      }
      public function set_phone_number($phoneNumber=""){
       $this->phone_number = $phoneNumber;
      }
      public function set_profile_picture($picture=""){
       $this->profile_picture = $picture;
      }
      public function set_skills($skills=""){
       $this->skills = $skills;
      }
      public function set_hobbies($hobbies=""){
       $this->hobbies = $hobbies;
      }
      public function set_grad_year($year=""){
       $this->grad_year = $year;
      }
      public function set_role($role=""){
       $this->role = $role;
      }
      
      
            
          
    }



   $user = new User();

?>
<?php

    class MessageService {
	//TODO find another approach of storing and quering conversations from database
	
	
	
       /*arranging messages to groups of sender messages*/
       public function arrange($messages=""){
	
        $arranged_messages = array();
	if($messages != NULL){
	    if(is_object($messages)){
		$arranged_messages[$messages->get_user_sender_id().','.
				     $messages->get_user_receiver_id()] = array(
			                                                    $messages
									);
	    }else{
	      foreach($messages as $message){
		if(!array_key_exists($message->get_user_sender_id().','.
				     $message->get_user_receiver_id(),$arranged_messages)){
		
		$arranged_messages[$message->get_user_sender_id().','.
				     $message->get_user_receiver_id()] = array(
			                                                    $message
									);
		}else{
		   array_push($arranged_messages[$message->get_user_sender_id().','.
				     $message->get_user_receiver_id()],$message);   
		}
	}
	    }
	}
	
	return $arranged_messages;
       }
       
       
       public function loadConversations($messages=""){
        $message_conversations = array();
        if($messages != NULL){
          $current_user_messages = array();
          
	  /*taking all messages sent by current login user*/
          foreach($messages as $messagegroup){
            if($messagegroup[0]->get_user_sender_id() == $_SESSION['user_id']){
                    $current_user_messages[$messagegroup[0]->get_user_receiver_id()] = $messagegroup;
                
            }
          }
	  $used_in_conversation = array();
	  /*if there are messages sent by current logged user*/
          if($current_user_messages != NULL){

          foreach($messages as $messagegroup){
            foreach($current_user_messages as $current_user_group){
		/* checking if it same group, then do not create a conversation*/
		if($messagegroup[0]->get_user_sender_id() !=
		   $current_user_group[0]->get_user_sender_id()){
                if($messagegroup[0]->get_user_sender_id() ==
		   $current_user_group[0]->get_user_receiver_id() ){
                    $message_conversation = $this->mergeByDate($messagegroup,$current_user_group);
		    array_push($used_in_conversation,$current_user_group);
                    array_push($message_conversations, $message_conversation);
                    break;
                }else if(!($this->containsMessage($current_user_messages,
					  $messagegroup[0]->get_user_sender_id()))){
		    array_push($message_conversations, $messagegroup);
		    break;
		}
		}
            }
	    
            
          }
	  
	  if($used_in_conversation != NULL){
	  foreach($current_user_messages as $current_user_group){
	   
		if(!in_array($current_user_group,$used_in_conversation)){
		     array_push($message_conversations,$current_user_group);
		}
	  }
	  }
	  }else{
	    
	    $message_conversations = $messages;
	  }
	  $message_conversations = $this->sortConversationsByDate($message_conversations);
	  
          return $message_conversations;
        }
       }
       
       private function mergeByDate($first_group="",$second_group=""){
        $merged_group = array();
	if(isset($first_group) && isset($second_group)
	   && $first_group != NULL && $second_group != NULL){
	    foreach($second_group as $message){
	    array_push($merged_group, $message);
	   }
	   foreach($first_group as $message){
	    array_push($merged_group, $message);
	   }
	   
	  
	   $merged_group = $this->sortByDate($merged_group);
	}
        
        
        return $merged_group;
        
       }
       private function sortByDate($group=""){
	if(isset($group) && $group != NULL){
	 usort($group,array(
			     $this,"compareDate"));
	       
           return $group;
	}
	
       }
       private function sortConversationsByDate($conversations=""){
	if(isset($conversations) && $conversations!= NULL){
	
	usort($conversations,array(
	                      $this,"compareConvDate"
	                       )
	      );
	return $conversations;
       }
       }
       
        private function compareDate($message1,$message2){
		return strtotime($message1->get_date()) < strtotime($message2->get_date());
	}
	private function compareConvDate($conversation1,$conversation2){
		return strtotime($conversation1[0]->get_date()) < strtotime($conversation2[0]->get_date());
	}
	private function containsMessage($array="",$sender_id=""){
	    foreach($array as $group){
		if($group[0]->get_user_receiver_id() == $sender_id){
		    return true;
		}
	    }
	    return false;
	}
       
       
       
    }
    

?>
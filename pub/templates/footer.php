<div class="u_footer navbar-bottom">
<div class="footer_content">
<div class="u_social">
<ul class=" nav u_footer_nav">
<li>
<?php echo (CurrentPage::$currentPage == "home")?'<a href="https://www.facebook.com/groups/onlyc4.cit.2012.2016/" target="_blank"> <img src="./pub/img/fb-icon.jpg" alt="facebook logo" width="20" height="20" /> Facebook </a>':(
	   (CurrentPage::$currentPage == "userhome" || CurrentPage::$currentPage == "messages" || CurrentPage::$currentPage == "userposts")?
	   '<a href="https://www.facebook.com/groups/onlyc4.cit.2012.2016/" target="_blank"> <img src="../../pub/img/fb-icon.jpg" alt="facebook logo" width="20" height="20" /> Facebook </a>':
	   ((isset($_GET['url']) && substr_count($_GET['url'],"/") ==3))?'<a href="https://www.facebook.com/groups/onlyc4.cit.2012.2016/" target="_blank" > <img src="../../pub/img/fb-icon.jpg" alt="facebook logo" width="20" height="20" /> Facebook </a>':
	   '<a href="https://www.facebook.com/groups/onlyc4.cit.2012.2016/" target="_blank" > <img src="../pub/img/fb-icon.jpg" alt="facebook logo" width="20" height="20" /> Facebook </a>');
?>
</li>
<li>
<?php
  echo (CurrentPage::$currentPage == "home")?'<a href="http://www.twitter.com/UICTCOMMUNITY" target="_blank"><img src="./pub/img/twitter-icon.jpg" alt="twitter logo" width="20" height="20" /> Twitter </a>':(
       (CurrentPage::$currentPage == "userhome" || CurrentPage::$currentPage == "messages" || CurrentPage::$currentPage == "userposts")?
       '<a href="http://www.twitter.com/UICTCOMMUNITY" target="_blank"><img src="../../pub/img/twitter-icon.jpg" alt="twitter logo" width="20" height="20" /> Twitter </a>':
	((isset($_GET['url']) && substr_count($_GET['url'],"/") ==3))?'<a href="http://www.twitter.com/UICTCOMMUNITY" target="_blank"><img src="../../pub/img/twitter-icon.jpg" alt="twitter logo" width="20" height="20" /> Twitter </a>':
	'<a href="http://www.twitter.com/UICTCOMMUNITY" target="_blank"><img src="../pub/img/twitter-icon.jpg" alt="twitter logo" width="20" height="20" /> Twitter </a>');
?>
</li>
<li>
<?php
   echo (CurrentPage::$currentPage == "home")?'<a href="https://plus.google.com/u/0/communities/100639855034148993539" target="_blank"><img src="./pub/img/google.jpg" alt="google+ logo"  width="20" height="20"/> Google+ </a>':(
	(CurrentPage::$currentPage == "userhome" || CurrentPage::$currentPage == "messages" || CurrentPage::$currentPage == "userposts")?
	'<a href="https://plus.google.com/u/0/communities/100639855034148993539" target="_blank"><img src="../../pub/img/google.jpg" alt="google+ logo"  width="20" height="20"/> Google+ </a>':
	((isset($_GET['url']) && substr_count($_GET['url'],"/") ==3))?'<a href="http://gplus.to/uict" target="_blank"><img src="../../pub/img/google.jpg" alt="google+ logo"  width="20" height="20"/> Google+ </a>':
	'<a href="https://plus.google.com/u/0/communities/100639855034148993539" target="_blank"><img src="../pub/img/google.jpg" alt="google+ logo"  width="20" height="20"/> Google+ </a>');
 ?>
</li>
</ul>
</div>
<div class="container">
<div class="u_links">
<ul class="nav u_footer_nav">
<li><a href="/home/about" >About</a></li>
<!--<li><a href="/uict/home/terms" >Terms</a></li>
<li><a href="/uict/home/privacy">Privacy</a></li>-->
<li><a href="/event/index" >Events</a></li>
<li><a href="/project/index" >Projects</a></li>
</ul>
</div>
<div class="u_links">
<ul class="nav u_footer_nav">
<!--<li><a href="/uict/event/index" >Events</a></li>
<li><a href="/uict/project/index" >Projects</a></li>-->
<!--<li><a href="/uict/home/contacts" >Contacts</a></li>-->
</ul>
</div>
</div>
</div>
<div class="copyright">
<div class="container">
<div class="nav pull-right ">
<p id="copyText" >&copy;<?php echo date("Y")?> UICT Community</p>
</div>
</div>
</div>

</div>

<?php
$loader = new Loader();
if (session_status() == PHP_SESSION_NONE) {
session_start();
}

$user_agent = isset( $_SERVER['HTTP_USER_AGENT'] ) ? $_SERVER['HTTP_USER_AGENT'] : '';






//if(!(isset($_SESSION['logged_in']) && $_SESSION['logged_in']== true)){
echo '
<header class="navbar navbar-static-top bs-docs-nav u_header" id="top" role="banner">
   ';
//}else {
//   echo '
//   <header class="navbar navbar-fixed-top bs-docs-nav u_header" id="top" role="banner">
//   ';
//}
   ?>

<div class="container">
   <?php
  if ( stristr( $user_agent, 'Opera' ) ) {
    echo '<button id="trigger" data-toggle="dropdown" class="u_n_button btn-navbar hidden-bar pull-right dropdown-toggle" type="button">
	   <span class="icon-fallback-text">
           <span class="icon icon-menu2" aria-hidden="true"></span>
           <span class="text">Menu</span>
           </span>
	</button>';
   }
   else{
   echo '<button id="trigger" data-toggle="dropdown" class="u_n_button btn-navbar hidden-bar pull-right dropdown-toggle" type="button">
	   <span class="glyphicon glyphicon-align-justify"></span>
	</button>';
   }
   ?>
 
  <!--<button type="button" class="dropdown btn btn-primary btn-navbar hidden-bar pull-right" >
      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
	  <span class="glyphicon glyphicon-align-justify"></span>
      </a>
  </button>-->
	      
   <?php
  
    if(CurrentPage::$currentPage == "home"){
       echo '<a href="/index.php" class=""><img class="logo img col-sm-4" src="./pub/img/uict.jpg" alt="uict logo"/></a>';
    }
    elseif(CurrentPage::$currentPage == "userhome"){

	      echo '<a href="/index.php" class=""><img class="logo img col-sm-4" src="../../pub/img/uict.jpg" alt="uict logo"/></a>';  	
    }elseif(isset($_GET['url']) && substr_count($_GET['url'],"/") ==2){
      if(CurrentPage::$currentPage == "registration"){
	  echo '<a href="/index.php" class=""><img class="logo img col-sm-4" src="../pub/img/uict.jpg" alt="uict logo"/></a>';  	
      }else{
	   echo '<a href="/index.php" class=""><img class="logo img col-sm-4" src="../../pub/img/uict.jpg" alt="uict logo"/></a>';  	   
      }
    }elseif(isset($_GET['url']) && substr_count($_GET['url'],"/") ==1){
	   echo '<a href="/index.php" class=""><img class="logo img col-sm-4" src="../pub/img/uict.jpg" alt="uict logo"/></a>';    
    }
    else{
       echo '<a href="/index.php" class=""><img class="logo img col-sm-4" src="../pub/img/uict.jpg" alt="uict logo"/></a>';  	
    }
   ?>
   
  
   
  <nav class="coll navbar-collapse" role="navigation">
	 <ul class="nav navbar navhide" id="navhide">
	   <li class= "<?php echo CurrentPage::$currentPage == "home"?'active':'list'; ?>" >
	      <?php
		   if(!(isset($_SESSION['logged_in']) && $_SESSION['logged_in']== true)){
		      echo '<a href="'.URL.'index.php">Home</a>';
		   }elseif(isset($_SESSION['user_id'])){
		      echo '<a href="'.URL.'home/userhome/'.$_SESSION['user_id'].'">Home</a>';	  
		   }
	      ?>
	      </li>
	      <li class= "<?php echo CurrentPage::$currentPage == "projects"?'active':'list'; ?>"><a href="<?php echo URL; ?>project/index" >Projects</a></li>
	      <li class= "<?php echo CurrentPage::$currentPage == "events"?'active':'list'; ?>"><a href="<?php echo URL; ?>event/index">Events</a></li>
	      <li class= "<?php echo CurrentPage::$currentPage == "about"?'active':'list'; ?>"><a href="<?php echo URL; ?>home/about">About</a></li>

	     
	      <?php
	      
		 if(!(isset($_SESSION['logged_in']) && $_SESSION['logged_in']== true)){
		    echo  '<li class="'.(CurrentPage::$currentPage == 'signin'?'active':'list').'"> <a href="'.URL.'login/index">Sign In</a></li>';
		 }else{
		  if(isset($data) && isset($data["newMessages"]) && count($data["newMessages"]) > 0 ){
		     echo '<li id="inbox_link" class="'.(CurrentPage::$currentPage == "messages"?'active':'list').'"><a href="'.URL.'user/messages/0">
		     <span class="message_note mnoteWithData1" >
		     '.count($data["newMessages"]).'
		     </span>Inbox
		     </a>
		     </li>';
		      }else{
			 echo '<li class="'.(CurrentPage::$currentPage == "messages"?'active':'list').'"><a href="'.URL.'user/messages/0">
			 <span class="message_note mnote1" >
			 </span>
			 Inbox</a>
			 </li>';
		      }
		     echo '<li class="'.(CurrentPage::$currentPage == "signin"?'active':'list').'"><a href="'.URL.'logout/auth">Sign Out</a></li>';
		 }
	      ?>
	      
	      <?php
	       if ( stristr( $user_agent, 'Opera' ) ) {
		  echo '
	      <li >
	      <a  class="toggleSearch toggleSearchFirst" >
	       <span class="icon-fallback-text">
               
	       <i title="click to search"  
		  class="icon icon-search searchIconFirst" aria-hidden="true" >Search
		  </i>
		  <span class="text">Search</span>
		  </span>
		  </a>
	      </li>';
	       }else{
	      echo '
	      <li ><a  class="toggleSearch toggleSearchFirst" >
	      <i  title="click to search"  
		  class="glyphicon glyphicon-search searchIconFirst"></i>
		  </a>
	      </li>';
	       }
	       ?>
      </ul>
       
 
       <ul class="nav navbar-nav navbar-right navbar-forhide">
	      <li class= "<?php echo CurrentPage::$currentPage == "home"?'active':'list'; ?>" >
	      <?php
		   if(!(isset($_SESSION['logged_in']) && $_SESSION['logged_in']== true)){
		      echo '<a href="'.URL.'index.php">Home</a>';
		   }elseif(isset($_SESSION['user_id'])){
		      echo '<a href="'.URL.'home/userhome/'.$_SESSION['user_id'].'">Home</a>';	  
		   }
	      ?>
	      </li>
	      <li class= "<?php echo CurrentPage::$currentPage == "projects"?'active':'list'; ?>"><a href="<?php echo URL; ?>project/index" >Projects</a></li>
	      <li class= "<?php echo CurrentPage::$currentPage == "events"?'active':'list'; ?>"><a href="<?php echo URL; ?>event/index">Events</a></li>
	      <li class= "<?php echo CurrentPage::$currentPage == "about"?'active':'list'; ?>"><a href="<?php echo URL; ?>home/about">About</a></li>
              
	     
	      <?php
	      
		 if(!(isset($_SESSION['logged_in']) && $_SESSION['logged_in']== true)){
		    echo  '<li class="'.(CurrentPage::$currentPage == 'signin'?'active':'list').'"> <a href="'.URL.'login/index">Sign In</a></li>';
		 }else{
		      if(isset($data) && isset($data["newMessages"]) && count($data["newMessages"]) > 0 ){
		     echo '<li id="inbox_link" class="'.(CurrentPage::$currentPage == "messages"?'active':'list').'"><a href="'.URL.'user/messages/0">
		     <span class="message_note mnoteWithData2" >
		     '.count($data["newMessages"]).'
		     </span>Inbox
		     </a></li>';
		      }else{
			 echo '<li class="'.(CurrentPage::$currentPage == "messages"?'active':'list').'"><a href="'.URL.'user/messages/0">
			 <span class="message_note mnote2"  >
			 </span>Inbox</a></li>';
		      }
		     echo '<li class="'.(CurrentPage::$currentPage == "signin"?'active':'list').'"><a href="'.URL.'logout/auth">Sign Out</a></li>';
		 }
	      ?>
	     <?php
	       if ( stristr( $user_agent, 'Opera' ) ) {
		  echo '
	      <li >
	      <a class="toggleSearch toggleSearchFirst" >
	       <span class="icon-fallback-text">
               
	       <i  title="click to search"  
		  class="icon icon-search searchIconFirst" aria-hidden="true" >Search
		  </i>
		  <span class="text">Search</span>
		  </span>
		  </a>
	      </li>';
	       }else{
	      echo '
	      <li ><a  class="toggleSearch toggleSearchFirst" >
	      <i  title="click to search"  
		  class="glyphicon glyphicon-search searchIconFirst"></i>
		  </a>
	      </li>';
	       }
	       ?>
	             </ul>
  </nav>
 </div>
     <div class="for_search">
	 <div class="container">
	   <div class="upper pull-right" >
	  <div id="search">
	      <form action="<?php echo URL?>home/siteSearch" method="get"  class="form-inline" role="form">
		  
		  <div class="input-group pull-right">
		      <ul class="nav">
			  
		      <li>
			<input class="form-control form-control-search" type="text" name="searchQuery" id="searchQuery" />
		    
		          <button class="input-group-addon" style="padding:7px 12px;">
		       search
		           </button>
		     </li>
		      </ul>
		  </div>
	       
		  
	      </form>
	  </div>
	  </div>
	  </div>
         
      </div>
      
</header>
        <?php
	if(!(!(isset($_SESSION['logged_in']) && $_SESSION['logged_in']== true))){
		  echo '
        <div class="container">
	<div class="in">
        <div class="col-md-2 col-xs-offset-10 inbox-messages">
	  
	    <div class="inbox_message_header">
		New Messages
	    </div>';
	    echo '
	    <div class="message_wrapper">
	    <div class="message_box">';
	    if(isset($data) && isset($data["newMessages"])){
	       foreach($data["newMessages"] as $message){
	    echo '<a class="rm_link" href="'.URL.'user/messages/'.$message->get_id().'">
	    <div class="message_div">
	    <div class="head_message">
	    From: '.$message->get_user_sender_name().'
	    </div>
	    <div class="s_message">'.$message->get_message().'</div>
	   
	    </div>
	    </a>';
	       }
	    }
	       echo '
	    </div>
	    </div>';
	    
	  echo '  
	  </div>
	</div>
	</div>';
	}
	?>
	
<div class="date">
	<?php
	echo '<div class="spanning">';
	echo '<span> Today Date:</span><br>';
	echo '</div>';
	
	echo '<div class="date-lg">';
	echo  "<b>".date('l jS,  m, Y ')."</b>";
	echo '</div>';
	?>
</div>
<div class="list-group">
	<!--<a href="#" class="list-group-item"><h3>Recent Activities</h3></a>
	<a href="#" class="list-group-item">Yesterday:</a>	
	<a href="#" class="list-group-item">This Week:</a>	
	<a href="#" class="list-group-item">Last Week:</a>-->
	
	<a href="#" class="list-group-item"><h3>Stats</h3></a>
	<a href="#" class="list-group-item">Messages:
	<?php
	   echo count((new Message())->get_all($_SESSION["user_id"]));
	?>
	</a>	
	<a href="#" class="list-group-item">Total Members:
	<?php
	   echo count((new User())->get_all());
	?>
	</a>	
	<a href="#" class="list-group-item">Total Projects:
	<?php
	   echo  count((new Project())->get_all());
	
	?>
	</a>
	<a href="#" class="list-group-item">Total Events:
	<?php
	   $loader = new Loader();
	   $loader->model("event.php");
	   echo  count((new Event())->get_all());
	
	?>
	</a>
 </div>
 

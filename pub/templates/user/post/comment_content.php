<?php
$comments = $data;
$output ="";
if($comments != NULL){
  if(is_array($comments)){
    foreach($comments as $comment){
       $user = (new User())->get_user($comment->get_user_id());
        if(!($user->get_id() == $_SESSION['user_id'])){
           $commenter = $user->get_fullName();
         }
         else{
          $commenter = 'Me';
          }
       $calcDate = date("Y-m-d",strtotime($comment->get_date_created()));
    if($calcDate == date("Y-m-d")){
       $displayDate = "Today at ".date("H:i:s",strtotime($comment->get_date_created()));
    }else{
       $displayDate = date("d-M-Y",strtotime($comment->get_date_created())).' at 
       '.date("H:i:s",strtotime($comment->get_date_created()));
    }
       $output .='
        <li class="comment " data-value="'.$comment->get_post_id().'">
             <div class="row">
             <div class="comment_image">';
              if($user->get_profile_picture() != NULL){
                 $output.= '<img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"     src="../../pub/img/userImages/'.$user->get_profile_picture().'" >';
               }else{
                 $output.= '<img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"  src="../../pub/img/avatars/profileImage.jpg" >';
               }
             
               $output .= '
               </div>
             <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
                 <h6 class="comment_head">'.$commenter.' commented</h5>
                 <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
               <p class="comment_content">
                '.$comment->get_comment().'
               </p>
               </div>
             </div>
             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
               <span style="font-size:50%;">
              '.$displayDate.'
              </span>
              </div>
             </div>
            
             </li>';
             (new Comment())->update($comment->get_id());
    }
   
  }else{
         $user = (new User())->get_user($comments->get_user_id());
         if(!($user->get_id() == $_SESSION['user_id'])){
           $commenter = $user->get_fullName();
         }
         else{
          $commenter = 'Me';
          }
       $output .='
        <li class="comment" data-value="'.$comment->get_post_id().'">
             <div class="row">
             <div class="comment_image">';
              if($user->get_profile_picture() != NULL){
                 $output.= '<img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"     src="../../pub/img/userImages/'.$user->get_profile_picture().'" >';
               }else{
                 $output.= '<img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"  src="../../pub/img/avatars/profileImage.jpg" >';
               }
               
            $output .=' </div>
             <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
                 <h6 class="comment_head">'.$commenter.' commented</h5>
                 <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
               <p class="comment_content">
                '.$comments->get_comment().'
               </p>
               </div>
             </div>
             <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
               <span style="font-size:50%;">
              '.$displayDate.'
              </span>
              </div>
             </div>
            
             </li>
             ';
             
             (new Comment())->update($comment->get_id());
  }
  }
  
  echo $output;
  
  
  ?>
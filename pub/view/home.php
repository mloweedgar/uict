<?php 
  /*
   require_once('../includes/model/session.php');
   require_once('../includes/helper/functions.php');

   if($session->is_logged_in == false){
       redirect('login.php');
   }
   */
?>
<?php
$loader = new Loader();

try{

   $loader->service('Template.php');
   $loader->service('CurrentPage.php');
   $loader->model("picture.php");
   $loader->model("comment.php");
}
catch(Exception $e){
 echo 'Message: '. $e->getMessage();
}



$template = new Template();

CurrentPage::$currentPage = "userhome";


?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Home | UICT Community</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php
	   try{
	       $template->render('resources.php');
	   }catch(Exception $e){
	       echo 'Message'.$e->getMessage();
	   }
 ?>
  
      
      
</head>
             
 <body>
	<div id="page">
	<div id="header">
	      <?php
		try{
		  $template->render('header.php',$data);
		}
		catch(Exception $e){
		  echo 'Message: '. $e->getMessage();
		}

	      ?>
	      </div>
	<div class="container">
	 <div class="row">

         <div class="col-md-3 visible-md visible-lg s_row ">
	  
             <div class="row user_photo">
	      <?php
	      if($data['user']->get_profile_picture() != NULL){
                  echo '<img class="img img-thumbnail" src="../../pub/img/userImages/'.$data['user']->get_profile_picture().'" />';
	      }else{
		      echo '<img class="img img-thumbnail" src="../../pub/img/avatars/profileImage.jpg" />';
	      }
	      ?>
			 <a href="<?php echo URL.'home/userProfile/'.$data['user']->get_id() ?>"
					title="Checkout Profile" ><?php echo $_SESSION['first_name'].' '.$_SESSION['last_name']; ?></a>
		 </div><!-- end of row for profile picture -->
		 <div class="row user_nav">
                   <?php
		    try{
		     $template->render('navigation.php',$data['posts']);
		    }catch(Exception $e){
		     echo 'Message:'.$e->getMessage();
		    }
		  ?>
		 </div><!-- end of row for info -->

         </div>
	 <!-- end of col-md-3 -->
	 <div class="container visible-sm visible-xs s_row">
	    <?php
		    try{
		     $dataToTemp = array(
					 'posts' =>$data['posts'],
					 'user' => $data['user']
					 );
		     $template->render('navigation_for_small.php',$dataToTemp);
		    }catch(Exception $e){
		     echo 'Message:'.$e->getMessage();
		    }
		  ?>
	 </div>
	 <!-- end of col-md-3 -->
   <div class="col-md-6 s_row">
       <div class="row ">
	       <div class="col-lg-12 col-md-12 ">
		  <div class="input-group">
		    <input type="text" id="searchIn" class="form-control searchIn" placeholder="Search for member">
		     <?php
		      echo '<div id="dataPage" style="display:hidden;"
		            data-value="'.CurrentPage::$currentPage.'"
			     >
			    </div>';
		     ?>
		     
		    <span class="input-group-btn">
		      <button class="btn u_s_button searchbuttonHeight" type="button">Search <span class="glyphicon glyphicon-search"></span></button>
		    </span>
		  </div><!-- /input-group -->
		</div><!-- /.col-lg-12 -->
		<div class="col-lg-12 searchResult" id="sResult">
		  <div class="users">
		     <ul class="nav" id="resultUl">
		     
		     </ul>
		  </div>
		  
		</div>
       </div>
       <!-- end of row for search bar -->
       <!--<div class="row visible-sm visible-xs">
	 <div class="col-sm-12 col-xs-12 ">
	 <div class="btn-group btn-group-justified">
	       <div class="btn-group">
		 <button type="button" class="btn btn-default">Search</button>
	       </div>
	       
	  </div>     
       </div>
       </div>-->
       <div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="post">
	 <textarea class="col-lg-12 col-md-12 col-sm-12 col-xs-12 postText" name="styled-textarea" id="styled" onfocus="this.value='';this.onfocus=null;
	 " >Write your post here...</textarea>
	 
	 <button class="btn btn-small pull-right postB" id="postButton" >Post</button>
	    
	    <ul class="nav attachments">
	       <li class="lis" >
	       <span id="uploadButton" class="btn btn-small pull-right uploadB" >
	      
	    <span class="glyphicon glyphicon-upload" ></span>
	    <span>Upload Image</span>
	     <div class='file_browse_wrapper'>
	       <input type='file' class='file_browse' id="infileImage">
	     </div>
	    </span>
	       </li>
	       
	      <!-- <li class="lis">
	    <span id="attachButton" class="btn btn-small pull-right attachB" >
	    <span class="glyphicon glyphicon-paperclip" ></span>
	    <span>Attach File</span>
	    <div class='file_browse_wrapper'>
	       
	       <input type='file' class='file_browse' id="infileFile">
	     </div>
	    </span>
	    </li>-->
	     
	    
	    <li class="lis" >
	       <ul class="nav">
		  
		  <li><progress id="imageProgress" value="0" min="0" max="100" ></progress> </li>
		  <li id="imageProgressNote"></li>
		  
		  
	       </ul>
	     
	    </li>
	    <!--<li class="lis" >
	       <ul class="nav">
		  
		  <li><progress id="fileProgress" value="0" min="0" max="100" ></progress></li>
		  <li id="fileProgressNote"></li>
		  
		  
	       </ul>
	  
	    </li>-->
	    </ul>
	    
	    
	 
	 
	 <span class="alert"></span>
	 <img class="img load"  src="../../pub/img/ui-trans.gif" />
	 
	    
	 
	    
	 
	 
	 
      </div>
   </div>
   
	 
       </div>

      <div class="story_form">
	 <div class="posts">
	     <?php
	     $userPosts = array();
	      $output = '';
	      if(isset($data['posts']) && $data['posts']!= NULL){
	          foreach($data['posts'] as $post){
                     if($post->get_user_id() == $_SESSION['user_id']){
                        array_push($userPosts,$post);
                     }
                  }
	      }
	       
	    
	     if(isset($data['posts']) && $data['posts']!= NULL){
	       
	       $i = 0;
	       //TODO create template for the below codes
	       
	      if(is_array($data['posts'])){
	      foreach($data['posts'] as $post){
	      $user = (new User())->get_user($post->get_user_id());
	      $comments = (new Comment())->get_by_post_id($post->get_id());
	      
	      
	      $output .='
	      <div class="post_div story_list">
	       <h3 class="title">
		 <span>';
		 if($user->get_profile_picture() != NULL){
		   $output.= '<img class="img post_img col-lg-2 col-md-2 col-sm-1 col-xs-3"         src="../../pub/img/userImages/'.$user->get_profile_picture().'" >';
		 }else{
		   $output.= '<img class="img post_img col-lg-2 col-md-2 col-sm-1 col-xs-3"  src="../../pub/img/avatars/profileImage.jpg" >';
		 }
		 $calcDate = date("Y-m-d",strtotime($post->get_date_created()));
		  if($calcDate == date("Y-m-d")){
		     $displayDate = "Today at ".date("H:i:s",strtotime($post->get_date_created()));
		  }else{
		     $displayDate = date("d-M-Y",strtotime($post->get_date_created())).' at 
		     '.date("H:i:s",strtotime($post->get_date_created()));
		  }
		  $output .= '
		  </span>';
		  if(!($user->get_id() == $_SESSION['user_id'])){
		     $output .= '<span class="widthper">
		          '.$user->get_fullName().' posted
		          </span>';
		 }
		 else{
		      $output .= '<span class="widthper">
		                   Me
		                  </span>';
		 }
		$output .='
		<span class="pull-right datepost widthperDate">
		  '.$displayDate.'
		 </span></h3>
		
		<ul class="nav div_cont" >
		  <li>
		  <p class="content">
		  '.$post->get_content().'
		  </p>
		  </li>
		</ul>';
		if($post->get_post_type() == "withImage"){
		  $picture = (new Picture())->get_picture($post->get_picture_id());
		  $output .= '
		<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<ul class="nav postUL">
		   <li>
		      <img class="postImage col-lg-6 col-md-6 col-sm-6 col-xs-6"  src="../.'.$picture->get_url().'">
		   </li>
		  
		</ul>
		</div>
		</div>';
		
		}
		if($post->get_post_type() == "withFile"){
		  $file = (new File())->get_file($post->get_file_id());
		  $output .= '
		<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<ul class="nav postUL">
		   <li>
		   <div class="story_list download_div">
		       <a class="no_link" href="'.$file.get_url().'">
		       Attached Document ,
		       You Download By Clicking this Link
		       </a>
		   </div>
		   
		   </li>
		  
		</ul>
		</div>
		</div>';
		
		}
		if($comments != NULL){
		if(is_array($comments)){
		  $output .='
		    <div class="row">
		       <div class="comments" >
		       <ul  class="nav comment-ul" id="comm'.$post->get_id().'" >';
		  foreach($comments as $comment){
		     $user = (new User())->get_user($comment->get_user_id());
		      if(!($user->get_id() == $_SESSION['user_id'])){
		         $commenter = $user->get_fullName();
		       }
		       else{
		        $commenter = 'Me';
		        }
		     $calcDate = date("Y-m-d",strtotime($comment->get_date_created()));
		  if($calcDate == date("Y-m-d")){
		     $displayDate = "Today at ".date("H:i:s",strtotime($comment->get_date_created()));
		  }else{
		     $displayDate = date("d-M-Y",strtotime($comment->get_date_created())).' at 
		     '.date("H:i:s",strtotime($comment->get_date_created()));
		  }
		     $output .='
		      <li class="comment">
			   <div class="row">
			   <div class="comment_image">';
			      if($user->get_profile_picture() != NULL){
				 $output.= '<img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"     src="../../pub/img/userImages/'.$user->get_profile_picture().'" >';
			       }else{
				 $output.= '<img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"  src="../../pub/img/avatars/profileImage.jpg" >';
			       }
			   
			   $output .= '
			   </div>
			   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
			       <h6 class="comment_head">'.$commenter.' commented</h5>
			       <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
			     <p class="comment_content">
			      '.$comment->get_comment().'
			     </p>
			     </div>
			   </div>
			   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                             <span style="font-size:50%;">
                            '.$displayDate.'
                            </span>
                            </div>
			   </div>
			  
			   </li>';
			   
                          (new Comment())->update($comment->get_id());
		  }
		  $output .= '
		  </ul>
		    </div>
		    </div>';
		}else{
		  $output .='
		    <div class="row">
		       <div class="comments"  >
		       <ul  class="nav comment-ul" id="comm'.$post->get_id().'" >';
		       $user = (new User())->get_user($comments->get_user_id());
		       if(!($user->get_id() == $_SESSION['user_id'])){
		         $commenter = $user->get_fullName();
		       }
		       else{
		        $commenter = 'Me';
		        }
		     $output .='
		      <li class="comment">
			   <div class="row">
			   <div class="comment_image">';
			   if($user->get_profile_picture() != NULL){
			      $output.= '<img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"     src="../../pub/img/userImages/'.$user->get_profile_picture().'" >';
			    }else{
			      $output.= '<img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"  src="../../pub/img/avatars/profileImage.jpg" >';
			    }
			    
			   $output .='
			   </div>
			   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
			       <h6 class="comment_head">'.$commenter.' commented</h5>
			       <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
			     <p class="comment_content">
			      '.$comments->get_comment().'
			     </p>
			     </div>
			   </div>
			   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                             <span style="font-size:50%;">
                            '.$displayDate.'
                            </span>
                            </div>
			   </div>
			  
			   </li>
			   </ul>
		    </div>
		    </div>';
		}
		}
		
		
		$output .='
	       <div class="dropdown" id="dropdown'.$post->get_id().'" >
	       <ul class="nav nav-pills content_nav comment-ul">
		 <li class="dropdown-toggle text_nav" data-toggle="dropdown" data-value="'.$post->get_id().'">
		   <a href="#" >
		   <span class="glyphicon glyphicon-comment"></span> Comment
		 </a>
		 
		 </li>
		
		
		</ul>
		<ul class="nav" style="display:none;" id="drop'.$post->get_id().'" >
		 
		   <li>
		   <div class="col-md-6">
		   <textarea class="comment_content" name="comment_content" onfocus="this.value=null;this.onfocus=null;" >
		   </textarea>
		   <br>
		   <div class="fmessage">
		     <button class="btn btn-small pull-left comment" >Comment</button>
		     <span class="pull-right notification" id="notification'.$post->get_id().'" style="display:none;"></span>
		   </div>
		   </div>
	       
		   </li>
		   
	   
	        </ul>
	 
	   
	        
		</div>';
		
		$output .='
		
	      </div>
	      ';
	       (new Post())->updateSeen($post->get_id());
	        
	      }
	      }else{
	       $post = $data['posts'];
	       
	      $user = (new User())->get_user($post->get_user_id());
	      $comments = (new Comment())->get_by_post_id($post->get_id());
	      
	      
	      $output .='
	      <div class="post_div story_list">
	       <h3 class="title">
		 <span>';
		 if($user->get_profile_picture() != NULL){
		   $output.= '<img class="img post_img col-lg-2 col-md-2 col-sm-1 col-xs-3"         src="../../pub/img/userImages/'.$user->get_profile_picture().'" >';
		 }else{
		   $output.= '<img class="img post_img col-lg-2 col-md-2 col-sm-1 col-xs-3"  src="../../pub/img/avatars/profileImage.jpg" >';
		 }
		 $calcDate = date("Y-m-d",strtotime($post->get_date_created()));
		  if($calcDate == date("Y-m-d")){
		     $displayDate = "Today at ".date("H:i:s",strtotime($post->get_date_created()));
		  }else{
		     $displayDate = date("d-M-Y",strtotime($post->get_date_created())).' at 
		     '.date("H:i:s",strtotime($post->get_date_created()));
		  }
		  $output .= '
		  </span>';
		  if(!($user->get_id() == $_SESSION['user_id'])){
		     $output .= '<span class="widthper">
		          '.$user->get_fullName().' posted
		          </span>';
		 }
		 else{
		      $output .= '<span class="widthper">
		                   Me
		                  </span>';
		 }
		$output .='
		<span class="pull-right datepost widthperDate">
		  '.$displayDate.'
		 </span></h3>
		
		<ul class="nav div_cont" >
		  <li>
		  <p class="content">
		  '.$post->get_content().'
		  </p>
		  </li>
		</ul>';
		if($post->get_post_type() == "withImage"){
		  $picture = (new Picture())->get_picture($post->get_picture_id());
		  $output .= '
		<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<ul class="nav postUL">
		   <li>
		      <img class="postImage col-lg-6 col-md-6 col-sm-6 col-xs-6"  src="../.'.$picture->get_url().'">
		   </li>
		  
		</ul>
		</div>
		</div>';
		
		}
		if($post->get_post_type() == "withFile"){
		  $file = (new File())->get_file($post->get_file_id());
		  $output .= '
		<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<ul class="nav postUL">
		   <li>
		   <div class="story_list download_div">
		       <a class="no_link" href="'.$file.get_url().'">
		       Attached Document ,
		       You Download By Clicking this Link
		       </a>
		   </div>
		   
		   </li>
		  
		</ul>
		</div>
		</div>';
		
		}
		if($comments != NULL){
		if(is_array($comments)){
		  $output .='
		    <div class="row">
		       <div class="comments" >
		       <ul  class="nav comment-ul" id="comm'.$post->get_id().'" >';
		  foreach($comments as $comment){
		     $user = (new User())->get_user($comment->get_user_id());
		      if(!($user->get_id() == $_SESSION['user_id'])){
		         $commenter = $user->get_fullName();
		       }
		       else{
		        $commenter = 'Me';
		        }
		     $calcDate = date("Y-m-d",strtotime($comment->get_date_created()));
		  if($calcDate == date("Y-m-d")){
		     $displayDate = "Today at ".date("H:i:s",strtotime($comment->get_date_created()));
		  }else{
		     $displayDate = date("d-M-Y",strtotime($comment->get_date_created())).' at 
		     '.date("H:i:s",strtotime($comment->get_date_created()));
		  }
		     $output .='
		      <li class="comment">
			   <div class="row">
			   <div class="comment_image">';
			      if($user->get_profile_picture() != NULL){
				 $output.= '<img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"     src="../../pub/img/userImages/'.$user->get_profile_picture().'" >';
			       }else{
				 $output.= '<img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"  src="../../pub/img/avatars/profileImage.jpg" >';
			       }
			    
			   $output .='
			   </div>
			   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
			       <h6 class="comment_head">'.$commenter.' commented</h5>
			       <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
			     <p class="comment_content">
			      '.$comment->get_comment().'
			     </p>
			     </div>
			   </div>
			   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                             <span style="font-size:50%;">
                            '.$displayDate.'
                            </span>
                            </div>
			   </div>
			  
			   </li>';
			   
                          (new Comment())->update($comment->get_id());
		  }
		  $output .= '
		  </ul>
		    </div>
		    </div>';
		}else{
		  $output .='
		    <div class="row">
		       <div class="comments"  >
		       <ul  class="nav comment-ul" id="comm'.$post->get_id().'" >';
		       $user = (new User())->get_user($comments->get_user_id());
		       if(!($user->get_id() == $_SESSION['user_id'])){
		         $commenter = $user->get_fullName();
		       }
		       else{
		        $commenter = 'Me';
		        }
		     $output .='
		      <li class="comment">
			   <div class="row">
			   <div class="comment_image">';
			      if($user->get_profile_picture() != NULL){
				 $output.= '<img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"     src="../../pub/img/userImages/'.$user->get_profile_picture().'" >';
			       }else{
				 $output.= '<img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"  src="../../pub/img/avatars/profileImage.jpg" >';
			       }
			 
			   $output .= '
			   </div>
			   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
			       <h6 class="comment_head">'.$commenter.' commented</h5>
			       <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
			     <p class="comment_content">
			      '.$comments->get_comment().'
			     </p>
			     </div>
			   </div>
			   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                             <span style="font-size:50%;">
                            '.$displayDate.'
                            </span>
                            </div>
			   </div>
			  
			   </li>
			   </ul>
		    </div>
		    </div>';
		}
		}
		
		
		$output .='
	       <div class="dropdown" id="dropdown'.$post->get_id().'" >
	       <ul class="nav nav-pills content_nav comment-ul">
		 <li class="dropdown-toggle text_nav" data-toggle="dropdown" data-value="'.$post->get_id().'">
		   <a href="#" >
		   <span class="glyphicon glyphicon-comment"></span> Comment
		 </a>
		 
		 </li>
		
		
		</ul>
		<ul class="nav" style="display:none;" id="drop'.$post->get_id().'" >
		 
		   <li>
		   <div class="col-md-6">
		   <textarea class="comment_content" name="comment_content" onfocus="this.value=null;this.onfocus=null;" >
		   </textarea>
		   <br>
		   <div class="fmessage">
		     <button class="btn btn-small pull-left comment" >Comment</button>
		     <span class="pull-right notification" id="notification'.$post->get_id().'" style="display:none;"></span>
		   </div>
		   </div>
	       
		   </li>
		   
	   
	        </ul>
	 
	   
	        
		</div>';
		
		$output .='
		
	      </div>
	      ';
	       (new Post())->updateSeen($post->get_id());
	       
	       
	       
	      }
	     
	      }
	       
	       
	       echo $output;
	       echo '</div>';
	        if(isset($data['posts']) && $data['posts']!= NULL){
	       echo '
	       <div class="story_list post_load" >
                   Load More Posts ...
                </div> ';
		}
	      ?>
	       
	 
	       
		<!-- All Stories list-->
		<?php
		$stories = $data['stories'];
		$latestUsers = $data['latestUsers'];
		$latestProjects = $data['latestProjects'];

		foreach($latestUsers as $user){
		 if($user['id']!=$_SESSION['user_id']){
		 echo '<div class="story_list">';
		 if($user['profile_picture'] != NULL){
		    echo '<img class="img col-lg-2 col-md-2 col-sm-2 col-xs-3"  src="../../pub/img/userImages/'.$user['profile_picture'].'" >';
		 }else{
		     echo '<img class="img col-lg-2 col-md-2 col-sm-2 col-xs-3"  src="../../pub/img/avatars/profileImage.jpg" >';
		 }
		 echo '<span class="widthper" >'.$user["first_name"].' '.$user["last_name"].'</span>';

		 echo '<p class="widthper div_cont" >Registered on '.date("d M Y",strtotime($user['registered_date'])).'</p>';
		 echo '<div class="story_picture pull-right">';

		 echo '</div>';
		 echo '<ul class="nav div_cont">';
		 echo '<li><span><i class="glyphicon glyphicon-file"></i>
		 <a href="/user/viewUser/'.$user['id'].'"  >    View Profile</a></span></li>';
		 echo '</ul>';
		 echo '</div>';
		 }
		}
		foreach($latestProjects as $project){
		 echo '<div class="story_list project">';
		 echo '<h4> '.strtoupper($project["title"]).'</h4>';
		 echo '<p>Project '.$project["title"].' is one of the latest projects</p>';
		 echo '<p>'.$project["description"].'</p>';
		 echo '<div class="story_picture pull-right">';

		 echo '</div>';
		 echo '<ul class="nav ">';
		 echo '<li><span><i class="glyphicon glyphicon-file"></i><a
		 href="/project/search?project_title='.$project["title"].'"
		        >    View</a></span></li>';
		 echo '</ul>';
		 echo '</div>';
		}



		?>
                 



			 </div><!-- end of row for user form -->

         </div><!-- end of col-md-6 -->
         <div class="col-md-3 visible-md visible-lg s_row">
            <?php
		try{
		  $template->render('left_side_menu.php');
		}
		catch(Exception $e){
		  echo 'Message: '. $e->getMessage();
		}

	      ?>
         </div><!-- end of col-md-3 -->

			 </div><!-- end u_main_content -->

			    </div>
		       </div>
		</div>
	       </div>
	</div>
	<div class="content">
	       <?php
		try{
		  $template->render('footer.php');
		}
		catch(Exception $e){
		  echo 'Message: '. $e->getMessage();
		}

	      ?>
	      </div>
	</div>
	<div class="hide" style="display:none;">
	 
	</div>
 </body>
</html>
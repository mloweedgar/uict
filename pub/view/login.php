
<?php //require_once('../includes/helper/initialize.php');

 /* now it is only require_once in first index.php
require_once('./includes/services/Loader.php');
*/
 
    $loader = new Loader();
    try{
       
       $loader->service('Template.php');
       $loader->service('CurrentPage.php');
    
       
       
       $template = new Template();
       
       CurrentPage::$currentPage = "signin";
       
    }catch(Exception $e){
       echo "Message: ".$e->getMessage();
    }
    
    $error = $data;
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Login | UICT Community</title>

	        <?php
		  try{
		      $template->render('resources.php');
		  }catch(Exception $e){
		      echo 'Message'.$e->getMessage();
		  }
		   
		  ?>

</head>
<body>
	<div id="page">
                <div id="header">
		    <?php
		      try{
			$template->render('header.php');
		      }
		      catch(Exception $e){
			echo 'Message: '. $e->getMessage();
		      }
		    
		    ?>
		    </div>
  <div class="row">
   <div  class="visible-lg login-div" >
     <div class="login-header" >    <!--the small uict logo was here before, it is replaced with login message now! -->
	 
	      <h3>Sign in</h3>
	 </div>    
     
	 <div class="ui_form">
	    <div class="error_message_login">
	        <?php
		   if(isset($error)){
		     echo $error;
		   }
		?>
            </div>
	    <form name="login" action="<?php echo URL?>login/auth" method="post">
	   
	      <input type="text" name="reg_number"  id="reg_number" required="" placeholder="Registration number"
		     class="form-control sensitive_info">
	    
	      <input type="password" name="password"   id="password" required="" placeholder="Password"
		     class="form-control sensitive_info">
	      
	      <input type="submit" value="Login" id="login" class="u_button login_button" >
	      <span><a href="<?php echo URL ?>home/resetPassword">Forgot password?</a></span>
	    </form>
	 </div>
   </div>
   <div  class="visible-md login-div-middle" >
     <div class="login-header" >    <!--the small uict logo was here before, it is replaced with login message now! -->
	 
	      <h3>Sign in</h3>
	 </div>    
     
	 <div class="ui_form">
	    <div class="error_message_login">
	        <?php
		   if(isset($error)){
		     echo $error;
		   }
		?>
            </div>
	    <form name="login" action="<?php echo URL?>login/auth" method="post">
	   
	      <input type="text" name="reg_number"  id="reg_number" required="" placeholder="Registration number"
		     class="form-control sensitive_info">
	    
	      <input type="password" name="password"   id="password" required="" placeholder="Password"
		     class="form-control sensitive_info">
	      
	      <input type="submit" value="Login" id="login" class="u_button login_button" >
	      <span><a href="<?php echo URL ?>home/resetPassword">Forgot password?</a></span>
	    </form>
	 </div>
   </div>
   <div  class="visible-sm login-div-small" >
     <div class="login-header" >    <!--the small uict logo was here before, it is replaced with login message now! -->
	 
	      <h3>Sign in</h3>
	 </div>    
     
	 <div class="ui_form">
	    <div class="error_message_login">
	        <?php
		   if(isset($error)){
		     echo $error;
		   }
		?>
            </div>
	    <form name="login" action="<?php echo URL?>login/auth" method="post">
	   
	      <input type="text" name="reg_number"  id="reg_number" required="" placeholder="Registration number"
		     class="form-control sensitive_info">
	    
	      <input type="password" name="password"   id="password" required="" placeholder="Password"
		     class="form-control sensitive_info">
	      
	      <input type="submit" value="Login" id="login" class="u_button login_button" >
	      <span><a href="<?php echo URL ?>home/resetPassword">Forgot password?</a></span>
	    </form>
	 </div>
   </div>
   <div  class="visible-xs login-div-xsmall" >
     <div class="login-header" >    <!--the small uict logo was here before, it is replaced with login message now! -->
	 
	      <h3>Sign in</h3>
	 </div>    
     
	 <div class="ui_form">
	    <div class="error_message_login">
	        <?php
		   if(isset($error)){
		     echo $error;
		     $error = null;
		   }
		?>
            </div>
	    <form name="login" action="<?php echo URL?>login/auth" method="post">
	   
	      <input type="text" name="reg_number"  id="reg_number" required="" placeholder="Registration number"
		     class="form-control sensitive_info">
	    
	      <input type="password" name="password"   id="password" required="" placeholder="Password"
		     class="form-control sensitive_info">
	      
	      <input type="submit" value="Login" id="login" class="u_button login_button" >
	      <span><a href="<?php echo URL ?>home/resetPassword">Forgot password?</a></span>
	    </form>
	 </div>
   </div>
   </div>
      <div class="content">
	      <?php
		  try{
		     $template->render('footer.php');
		  }catch(Exception $e){
		     echo "Message: ".$e->getMessage();
		  }
	      ?>
		
      </div><!-- end of content -->

    </div>
  
</body>
</html>

<?php 
  /*
   require_once('../includes/model/session.php');
   require_once('../includes/helper/functions.php');

   if($session->is_logged_in == false){
       redirect('login.php');
   }
   */
?>
<?php
$loader = new Loader();

try{

   $loader->service('Template.php');
   $loader->service('CurrentPage.php');
}
catch(Exception $e){
 echo 'Message: '. $e->getMessage();
}



$template = new Template();

CurrentPage::$currentPage = "messages";


?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Home | UICT Community</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php
	   try{
	       $template->render('resources.php');
	   }catch(Exception $e){
	       echo 'Message'.$e->getMessage();
	   }
 ?>
             
 <body>
	<div id="page">
	<div id="header">
	      <?php
		try{
		  $template->render('header.php',$data);
		}
		catch(Exception $e){
		  echo 'Message: '. $e->getMessage();
		}

	      ?>
	      </div>
	<div class="container">
	 <div class="row s_row">

         <div class="col-md-3 visible-md visible-lg">
	  
             <div class="row user_photo">
	      <?php
	      if($data['user']->get_profile_picture() != NULL){
                  echo '<img class="img img-thumbnail" src="../../pub/img/userImages/'.$data['user']->get_profile_picture().'" />';
	      }else{
		      echo '<img class="img img-thumbnail" src="../../pub/img/avatars/profileImage.jpg" />';
	      }
	      ?>
			 <a href="<?php echo URL.'home/userProfile/'.$data['user']->get_id() ?>"
					title="Checkout Profile" ><?php echo $_SESSION['first_name'].' '.$_SESSION['last_name']; ?></a>
		 </div><!-- end of row for profile picture -->
		 <div class="row user_nav">
                   <?php
		    try{
		     $template->render('navigation.php',$data['posts']);
		    }catch(Exception $e){
		     echo 'Message:'.$e->getMessage();
		    }
		  ?>
		 </div><!-- end of row for info -->

         </div><!-- end of col-md-3 -->
	 <div class="container visible-sm visible-xs s_row">
	    <?php
		    try{
		     $dataToTemp = array(
					 'posts' =>$data['posts'],
					 'user' => $data['user']
					 );
		     $template->render('navigation_for_small.php',$dataToTemp);
		    }catch(Exception $e){
		     echo 'Message:'.$e->getMessage();
		    }
		  ?>
	 </div>
         <div class="col-md-6 ">
             <div class="row">
	             <div class="col-lg-12">
			<div class="input-group">
			  <input type="text" class="form-control" placeholder="Search for member">
			  <?php
			   echo '<div id="dataPage" style="display:hidden;"
				 data-value="'.CurrentPage::$currentPage.'"
				  >
				 </div>';
		           ?>
			  <span class="input-group-btn">
			    <button class="btn u_s_button searchbuttonHeight" type="button">Search <span class="glyphicon glyphicon-search"></span></button>
			  </span>
			</div><!-- /input-group -->
		      </div><!-- /.col-lg-6 -->
	     </div><!-- end of row for search bar -->
	 </div>
	<!-- <div class="visible-sm visible-xs">
	 <div class="col-sm-12 col-xs-12 ">
	 <div class="btn-group btn-group-justified">
	       <div class="btn-group">
		 <button type="button" class="btn btn-default">Search</button>
	       </div>
	       
	  </div>     
       </div>
       </div>-->
	 <div class="col-md-8">
	     <div class="row user_form">
		<!-- All Stories list-->
		<?php
		
      if(isset($data["messages"]) && $data["messages"]!=NULL){
      foreach($data["messages"] as $message_group){
	if(count($message_group) == 1) {
	  echo '<div class="story_list">';
         $conversation_receiver ="";
	if($message_group[0]->get_user_sender_id() == $_SESSION['user_id']){
	    $conversation_receiver = $data['user']->get_user($message_group[0]->get_user_receiver_id());
	}else{
	    $conversation_receiver = $data['user']->get_user($message_group[0]->get_user_sender_id());
	}
	if($conversation_receiver->get_profile_picture()!= NULL){
	   echo '<img class="img col-sm-1 img-custom"  src="../../pub/img/userImages/'.$conversation_receiver->get_profile_picture().'" >';
	}else{
	    echo '<img class="img col-sm-1 img-custom"  src="../../pub/img/avatars/profileImage.jpg" >';
	}
	echo '<div class="row">';
       echo '<div class="col-md-10">';
	echo '
	     <div class="message_div">
	     <ul class="nav">
	     <li class="f_message_in"> Conversation with '.$conversation_receiver->get_fullName().'</li>
	     <div class="message_lists" >
	     ';
	     if($message_group[0]->get_user_sender_id() != $data['user']->get_id()){
	     if($message_group[0]->get_seen() == true){
	        echo '<li class="s_message_in " id="ms'.$message_group[0]->get_id().'">';
	     }else{
	        echo '<li class="s_message_in new_ms" id="ms'.$message_group[0]->get_id().'">';
		echo '<ul class="nav">
		        <li class="new_not pull-left">new</li>
		     </ul>';
	     }
	     }else{
	       echo '<li class="s_message_in " id="ms'.$message_group[0]->get_id().'">';
	     }
	     //echo '
	     // <span class="glyphicon glyphicon-remove pull-right remove"
	     //data-toggle="tooltip" data-placement="right" title="Delete this message"
	     //id="remove'.$message_group[0]->get_id().'" data-value="'.$message_group[0]->get_id().'">
	     //</span>';
	     
	     if($conversation_receiver->get_id() == $_SESSION['user_id']){
	       echo '<strong>Me</strong>:
	       <span class="mes">'.$message_group[0]->get_message().'</span>
	        ';
	       
	     }else{
	       echo '<strong>'.$conversation_receiver->get_first_name().'</strong>:
	       <span class="mes">'.$message_group[0]->get_message().'</span>
	       ';
	     }
	     echo '<div class="alert-info" style="display:none;" id="inf'.$message_group[0]->get_id().'" >Problem deleting, please refresh page first</div>
	     ';
	     echo '<img class="img loadms" id="loadms'.$message_group[0]->get_id().'" src="../../pub/img/ui-trans.gif" />';
	     
	     
	     echo '  <span class="pull-right time_received">'.date("l M d, Y",strtotime($message_group[0]->get_date())).'
	    <span class=""> '.date("H:i:s",strtotime($message_group[0]->get_date())).'</span></span></li>
	     </div>
             </ul>
	     </div>';
	     
	     if($conversation_receiver->get_id() != $_SESSION['user_id']){
	       
	       echo '
	     
	     <ul class="nav" style="display:none;" id="drop'.$conversation_receiver->get_id().'" >
  
	       <li>
	       <div class="col-md-5 col-xs-offset-1">
	       <textarea class="message_content" name="message_content" rows="3" cols="40" onfocus="this.value=null;this.onfocus=null;" >
	       </textarea><br>
	       <div class="">
		 <button class="btn btn-small pull-left send" >Send</button>
		 <span class="pull-right notification"
		 id="notification'.$conversation_receiver->get_id().'" style="display:none;"></span>
	       </div>
	       </div>
	   
	       </li>
	       
	     
	    </ul>';
	echo '<ul class="nav in-messages">';
	echo '<li><span><a href="#" class="text_nav" data-value="'.$conversation_receiver->get_id().'">Reply</a></span></li>';
	echo '<li><span><a href="#" class="delete_all" >Delete All</a></span></li>';
	echo '</ul>';
	
	     }
	     echo '</div>';
	echo '</div>';
	echo '</div>';
       
	}else{
	echo '<div class="story_list">';
	$conversation_receiver = "";
	$lone = true;
	foreach($message_group as $message){
	   if($message->get_user_sender_id() != $_SESSION['user_id']){
	    $conversation_receiver = $message->get_userObj();
	    $lone = false;
	    break;
	   }
	}
	if($lone){
	 $conversation_receiver = $data['user']->get_user($message_group[0]->get_user_receiver_id());
	}
	
	if($conversation_receiver->get_profile_picture() != NULL){
	  
	   echo '<img class="img col-sm-1 img-custom"  src="../../pub/img/userImages/'. $conversation_receiver->get_profile_picture().'" >';
	   
	}else{
	    echo '<img class="img col-sm-1 img-custom"  src="../../pub/img/avatars/profileImage.jpg" >';
	   
	}
	echo '<div class="row">';
       echo '<div class="col-md-10">';
	echo '
	     <div class="message_div">
	     
	      <ul class="nav">
	      <li class="f_message_in"> Conversation with '.$conversation_receiver->get_fullName().' </li>
	       <div class="message_lists" >
	    
	     ';
        foreach($message_group as $message){
	    if($message->get_user_sender_id() != $data['user']->get_id()){
	      
	      if($message->get_seen() == true){
	        echo '<li class="s_message_in  received" id="ms'.$message->get_id().'">';
	     }else{
	        echo '<li class="s_message_in new_ms received" id="ms'.$message->get_id().'">';
		echo '<ul class="nav">
		        <li class="new_not pull-left">new</li>
		     </ul>';
	     }
	      
	     //echo '
	     //<span class="glyphicon glyphicon-remove pull-right remove"
	     //data-toggle="tooltip" data-placement="right" title="Delete this message"
	     //id="remove'.$message->get_id().'" data-value="'.$message->get_id().'">
	     //</span>';
	     echo '
	     
	     <strong>'.$conversation_receiver->get_first_name().'</strong>:  <span class="mes">'.$message->get_message().'</span>
	     <div class="alert-info" style="display:none;" id="inf'.$message->get_id().'" >Problem deleting, please refresh page first</div>
	     <img class="img loadms"  id="loadms'.$message->get_id().'" src="../../pub/img/ui-trans.gif" />
	     
	     <span class="pull-right time_received">'.date("l M d, Y",strtotime($message->get_date())).'
	     
	     <span class=""> '.date("H:i:s",strtotime($message->get_date())).'</span></span>
	     </li>';
	    }else{
	        echo '<li class="s_message_in sent" id="ms'.$message->get_id().'">';
	     
	     // echo '
	     // <span class="glyphicon glyphicon-remove pull-right remove"
	     //data-toggle="tooltip" data-placement="right" title="Delete this message"
	     //id="remove'.$message->get_id().'" data-value="'.$message->get_id().'">
	     //</span>';
	     echo '
	     
	     <strong> Me</strong>:  <span class="mes"> '.$message->get_message().'</span>
	     <div class="alert-info" style="display:none;" id="inf'.$message->get_id().'" >Problem deleting, please refresh page first</div>
	     
	     <img class="img loadms" id="loadms'.$message->get_id().'" src="../../pub/img/ui-trans.gif" />
	     
	     <span class="pull-right time_received">'.date("l M d, Y",strtotime($message->get_date())).'
	     <span class=""> '.date("H:i:s",strtotime($message->get_date())).'</span></span></li>';
	    
	    }
	    }
	    echo '</div>
	    </ul>';
	    echo '</div>';
	    if(!$lone){
	       
	    echo '<ul class="nav" style="display:none;" id="drop'.$conversation_receiver->get_id().'" >
  
	       <li>
	       <div class="col-md-5 col-xs-offset-1">
	       <textarea class="message_content" name="message_content" rows="3" cols="40" onfocus="this.value=null;this.onfocus=null;" >
	       </textarea><br>
	       <div class="">
		 <button class="btn btn-small pull-left send" >Send</button>
		 <span class="pull-right notification"
		 id="notification'.$conversation_receiver->get_id().'" style="display:none;"></span>
	       </div>
	       </div>
	   
	       </li>
	       
	     
	    </ul>';
	echo '<ul class="nav in-messages">';
	echo '<li><span><a href="#" class="text_nav" data-value="'.$conversation_receiver->get_id().'" >Reply</a></span></li>';
	echo '<li><span><a href="#" class="delete_all" >Delete All</a></span></li>';
	echo '</ul>';
	
	
	    }
	 echo '</div>';
	echo '</div>';
	echo '</div>';
	
	}
      }
      }else{
	 echo '<div class="container">
	       <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
	         <h3>You have no messages in your inbox </h3>
	       </div>
	       </div>
	 ';
      }
       
       
		

 

		?>
                 



			 </div><!-- end of row for user form -->

         </div><!-- end of col-md-6 -->
        <!-- end of col-md-3 -->

			 </div><!-- end u_main_content -->

			    </div>
		       </div>
		</div>
	       </div>
	</div>
	<div class="content">
	       <?php
		try{
		  $template->render('footer.php');
		}
		catch(Exception $e){
		  echo 'Message: '. $e->getMessage();
		}

	      ?>
	      </div>
	</div>
	<?php
	   $messageToUpdate = new Message();
	   $messageToUpdate->setAllSeen($_SESSION["user_id"]);
	   
	?>
 </body>
</html>
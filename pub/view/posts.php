<?php 
  /*
   require_once('../includes/model/session.php');
   require_once('../includes/helper/functions.php');

   if($session->is_logged_in == false){
       redirect('login.php');
   }
   */
?>
<?php
$loader = new Loader();

try{

   $loader->service('Template.php');
   $loader->service('CurrentPage.php');
   $loader->model("picture.php");
   $loader->model("comment.php");
}
catch(Exception $e){
 echo 'Message: '. $e->getMessage();
}



$template = new Template();

CurrentPage::$currentPage = "userposts";


?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>User Posts | UICT Community</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php
	   try{
	       $template->render('resources.php');
	   }catch(Exception $e){
	       echo 'Message'.$e->getMessage();
	   }
 ?>
  
      
      
</head>
             
 <body>
	<div id="page">
	<div id="header">
	      <?php
		try{
		  $template->render('header.php',$data);
		}
		catch(Exception $e){
		  echo 'Message: '. $e->getMessage();
		}

	      ?>
	      </div>
	<div class="container">
	 <div class="row">

         <div class="col-md-3 visible-md visible-lg s_row ">
	  
             <div class="row user_photo">
	      <?php
	      if($data['user']->get_profile_picture() != NULL){
                  echo '<img class="img img-thumbnail" src="../../pub/img/userImages/'.$data['user']->get_profile_picture().'" />';
	      }else{
		      echo '<img class="img img-thumbnail" src="../../pub/img/avatars/profileImage.jpg" />';
	      }
	      ?>
			 <a href="<?php echo URL.'home/userProfile/'.$data['user']->get_id() ?>"
					title="Checkout Profile" ><?php echo $_SESSION['first_name'].' '.$_SESSION['last_name']; ?></a>
		 </div><!-- end of row for profile picture -->
		 <div class="row user_nav">
                   <?php
		    try{
		     $template->render('navigation.php',$data['posts']);
		    }catch(Exception $e){
		     echo 'Message:'.$e->getMessage();
		    }
		  ?>
		 </div><!-- end of row for info -->

         </div>
	 <!-- end of col-md-3 -->
	 <div class="container visible-sm visible-xs s_row">
	    <?php
		    try{
		     $dataToTemp = array(
					 'posts' =>$data['posts'],
					 'user' => $data['user']
					 );
		     $template->render('navigation_for_small.php',$dataToTemp);
		    }catch(Exception $e){
		     echo 'Message:'.$e->getMessage();
		    }
		  ?>
	 </div>
	 <!-- end of col-md-3 -->
   <div class="col-md-6 s_row">
       <div class="row ">
	       <div class="col-lg-12 col-md-12 ">
		  <div class="input-group">
		    <input type="text" id="searchIn" class="form-control searchIn" placeholder="Search for member">
		    <?php
		      echo '<div id="dataPage" style="display:hidden;"
		            data-value="'.CurrentPage::$currentPage.'"
			     >
			    </div>';
		     ?>
		    
		    <span class="input-group-btn">
		      <button class="btn u_s_button searchbuttonHeight" type="button">Search <span class="glyphicon glyphicon-search"></span></button>
		    </span>
		  </div><!-- /input-group -->
		</div><!-- /.col-lg-12 -->
		<div class="col-lg-12 searchResult" id="sResult">
		  <div class="users">
		     <ul class="nav" id="resultUl">
		     
		     </ul>
		  </div>
		  
		</div>
       </div>
       <!-- end of row for search bar -->
       <!--<div class="row visible-sm visible-xs">
	 <div class="col-sm-12 col-xs-12 ">
	 <div class="btn-group btn-group-justified">
	       <div class="btn-group">
		 <button type="button" class="btn btn-default">Search</button>
	       </div>
	       
	  </div>     
       </div>
       </div>-->
      

      <div class="story_form">
             <div class="post_lists"  > 
	     <?php
	     $userPosts = array();
	      $output = '';
              if($data['posts'] != NULL){
	       foreach($data['posts'] as $post){
                 if($post->get_user_id() == $_SESSION['user_id']){
                   array_push($userPosts,$post);
                  }
                 }
              }
	       
	    
	     if(isset($data['posts']) && $data['posts']!= NULL){
	       if(is_array($data['posts'])){
	       $i = 0;
	      foreach($data['posts'] as $post){
	      $user = (new User())->get_user($post->get_user_id());
	      $comments = (new Comment())->get_by_post_id($post->get_id());
	      
	      
	      $output .='
	      <div class="post_div story_list" id="ps'.$post->get_id().'" >
	       <h3 class="title">
		 <span>';
		 if($user->get_profile_picture() != NULL){
		   $output.= '<img class="img post_img col-lg-2 col-md-2 col-sm-1 col-xs-3"         src="../../pub/img/userImages/'.$user->get_profile_picture().'" >';
		 }else{
		   $output.= '<img class="img post_img col-lg-2 col-md-2 col-sm-1 col-xs-3"  src="../../pub/img/avatars/profileImage.jpg" >';
		 }
		 $calcDate = date("Y-m-d",strtotime($post->get_date_created()));
		  if($calcDate == date("Y-m-d")){
		     $displayDate = "Today at ".date("H:i:s",strtotime($post->get_date_created()));
		  }else{
		     $displayDate = date("d-M-Y",strtotime($post->get_date_created())).' at 
		     '.date("H:i:s",strtotime($post->get_date_created()));
		  }
		  $output .= '
                   </span>';
		  if(!($user->get_id() == $_SESSION['user_id'])){
		     $output .= '<span class="widthper">
		          '.$user->get_fullName().' posted
		          </span>';
		 }
		 else{
		      $output .= '<span class="widthper">
		                   Me
		                  </span>';
		 }
		$output .='
		<span class="pull-right datepost widthperDate">
		  '.$displayDate.'';
	       if($_SESSION["user_id"] == $post->get_user_id()){
                  $output .='
		  <span data-value="'.$post->get_id().'" style="padding-left:10px;" id="removepost'.$post->get_id().'"
                   title="" data-placement="right" data-toggle="tooltip"
                   class="glyphicon glyphicon-remove pull-right removepost"
                   data-original-title="Delete this post">
		  
		 </span>';
	         }
		 $output .='
		 </h3>
		
		<ul class="nav div_cont" >
		  <li>
		  <p class="content">
		  '.$post->get_content().'
		  </p>
		  </li>
		</ul>';
                $output .=  '<img src="../../pub/img/ui-trans.gif" id="loadps'.$post->get_id().'" class="img loadps">';
		if($post->get_post_type() == "withImage"){
		  $picture = (new Picture())->get_picture($post->get_picture_id());
		  $output .= '
		<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<ul class="nav postUL">
		   <li>
		      <img class="postImage col-lg-12 col-md-12 col-sm-12 col-xs-12"  src="../.'.$picture->get_url().'">
		   </li>
		  
		</ul>
		</div>
		</div>';
		
		}
		if($comments != NULL){
		if(is_array($comments)){
		  $output .='
		    <div class="row">
		       <div class="comments" >
		       <ul  class="nav comment-ul" id="comm'.$post->get_id().'" >';
		  foreach($comments as $comment){
		     $user = (new User())->get_user($comment->get_user_id());
		      if(!($user->get_id() == $_SESSION['user_id'])){
		         $commenter = $user->get_fullName();
		       }
		       else{
		        $commenter = 'Me';
		        }
		     $calcDate = date("Y-m-d",strtotime($comment->get_date_created()));
		  if($calcDate == date("Y-m-d")){
		     $displayDate = "Today at ".date("H:i:s",strtotime($comment->get_date_created()));
		  }else{
		     $displayDate = date("d-M-Y",strtotime($comment->get_date_created())).' at 
		     '.date("H:i:s",strtotime($comment->get_date_created()));
		  }
		     $output .='
		      <li class="comment">
			   <div class="row">
			   <div class="comment_image">
			     <img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"     src="../../pub/img/userImages/'.$user->get_profile_picture().'" >
			   </div>
			   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
			       <h6 class="comment_head">'.$commenter.' commented</h5>
			       <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
			     <p class="comment_content">
			      '.$comment->get_comment().'
			     </p>
			     </div>
			   </div>
			   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                             <span style="font-size:50%;">
                            '.$displayDate.'
                            </span>
                            </div>
			   </div>
			  
			   </li>';
			   
                          (new Comment())->update($comment->get_id());
		  }
		  $output .= '
		  </ul>
		    </div>
		    </div>';
		}else{
		  $output .='
		    <div class="row">
		       <div class="comments"  >
		       <ul  class="nav comment-ul" id="comm'.$post->get_id().'" >';
		       $user = (new User())->get_user($comments->get_user_id());
		       if(!($user->get_id() == $_SESSION['user_id'])){
		         $commenter = $user->get_fullName();
		       }
		       else{
		        $commenter = 'Me';
		        }
		     $output .='
		      <li class="comment">
			   <div class="row">
			   <div class="comment_image">
			     <img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"     src="../../pub/img/userImages/'.$user->get_profile_picture().'" >
			   </div>
			   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
			       <h6 class="comment_head">'.$commenter.' commented</h5>
			       <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
			     <p class="comment_content">
			      '.$comments->get_comment().'
			     </p>
			     </div>
			   </div>
			   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                             <span style="font-size:50%;">
                            '.$displayDate.'
                            </span>
                            </div>
			   </div>
			  
			   </li>
			   </ul>
		    </div>
		    </div>';
		}
		}
		
		
		$output .='
	       <div class="dropdown" id="dropdown'.$post->get_id().'" >
	       <ul class="nav nav-pills content_nav comment-ul">
		 <li class="dropdown-toggle text_nav" data-toggle="dropdown" data-value="'.$post->get_id().'">
		   <a href="#" >
		   <span class="glyphicon glyphicon-comment"></span> Comment
		 </a>
		 
		 </li>
		
		
		</ul>
		<ul class="nav" style="display:none;" id="drop'.$post->get_id().'" >
		 
		   <li>
		   <div class="col-md-6">
		   <textarea class="comment_content" name="comment_content" onfocus="this.value=null;this.onfocus=null;" >
		   </textarea>
		   <br>
		   <div class="fmessage">
		     <button class="btn btn-small pull-left comment" >Comment</button>
		     <span class="pull-right notification" id="notification'.$post->get_id().'" style="display:none;"></span>
		   </div>
		   </div>
	       
		   </li>
		   
	   
	        </ul>
	 
	   
	        
		</div>';
		
		$output .='
		
	      </div>';
	       (new Post())->updateSeen($post->get_id());
	        
	      }
	       }else{
		 $post = $data['posts'];  
	      $user = (new User())->get_user($post->get_user_id());
	      $comments = (new Comment())->get_by_post_id($post->get_id());
	      
	      
	      $output .='
	      <div class="post_div story_list" id="ps'.$post->get_id().'" >
	       <h3 class="title">
		 <span>';
		 if($user->get_profile_picture() != NULL){
		   $output.= '<img class="img post_img col-lg-2 col-md-2 col-sm-1 col-xs-3"         src="../../pub/img/userImages/'.$user->get_profile_picture().'" >';
		 }else{
		   $output.= '<img class="img post_img col-lg-2 col-md-2 col-sm-1 col-xs-3"  src="../../pub/img/avatars/profileImage.jpg" >';
		 }
		 $calcDate = date("Y-m-d",strtotime($post->get_date_created()));
		  if($calcDate == date("Y-m-d")){
		     $displayDate = "Today at ".date("H:i:s",strtotime($post->get_date_created()));
		  }else{
		     $displayDate = date("d-M-Y",strtotime($post->get_date_created())).' at 
		     '.date("H:i:s",strtotime($post->get_date_created()));
		  }
		  $output .= '
                   </span>';
		  if(!($user->get_id() == $_SESSION['user_id'])){
		     $output .= '<span class="widthper">
		          '.$user->get_fullName().' posted
		          </span>';
		 }
		 else{
		      $output .= '<span class="widthper">
		                   Me
		                  </span>';
		 }
		$output .='
		<span class="pull-right datepost widthperDate">
		  '.$displayDate.'';
	       if($_SESSION["user_id"] == $post->get_user_id()){
                  $output .='
		  <span data-value="'.$post->get_id().'" style="padding-left:10px;" id="removepost'.$post->get_id().'"
                   title="" data-placement="right" data-toggle="tooltip"
                   class="glyphicon glyphicon-remove pull-right removepost"
                   data-original-title="Delete this post">
		  
		 </span>';
	         }
		 $output .='
		 </h3>
		
		<ul class="nav div_cont" >
		  <li>
		  <p class="content">
		  '.$post->get_content().'
		  </p>
		  </li>
		</ul>';
                $output .=  '<img src="../../pub/img/ui-trans.gif" id="loadps'.$post->get_id().'" class="img loadps">';
		if($post->get_post_type() == "withImage"){
		  $picture = (new Picture())->get_picture($post->get_picture_id());
		  $output .= '
		<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<ul class="nav postUL">
		   <li>
		      <img class="postImage col-lg-12 col-md-12 col-sm-12 col-xs-12"  src="../.'.$picture->get_url().'">
		   </li>
		  
		</ul>
		</div>
		</div>';
		
		}
		if($comments != NULL){
		if(is_array($comments)){
		  $output .='
		    <div class="row">
		       <div class="comments" >
		       <ul  class="nav comment-ul" id="comm'.$post->get_id().'" >';
		  foreach($comments as $comment){
		     $user = (new User())->get_user($comment->get_user_id());
		      if(!($user->get_id() == $_SESSION['user_id'])){
		         $commenter = $user->get_fullName();
		       }
		       else{
		        $commenter = 'Me';
		        }
		     $calcDate = date("Y-m-d",strtotime($comment->get_date_created()));
		  if($calcDate == date("Y-m-d")){
		     $displayDate = "Today at ".date("H:i:s",strtotime($comment->get_date_created()));
		  }else{
		     $displayDate = date("d-M-Y",strtotime($comment->get_date_created())).' at 
		     '.date("H:i:s",strtotime($comment->get_date_created()));
		  }
		     $output .='
		      <li class="comment">
			   <div class="row">
			   <div class="comment_image">
			     <img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"     src="../../pub/img/userImages/'.$user->get_profile_picture().'" >
			   </div>
			   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
			       <h6 class="comment_head">'.$commenter.' commented</h5>
			       <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
			     <p class="comment_content">
			      '.$comment->get_comment().'
			     </p>
			     </div>
			   </div>
			   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                             <span style="font-size:50%;">
                            '.$displayDate.'
                            </span>
                            </div>
			   </div>
			  
			   </li>';
			   
                          (new Comment())->update($comment->get_id());
		  }
		  $output .= '
		  </ul>
		    </div>
		    </div>';
		}else{
		  $output .='
		    <div class="row">
		       <div class="comments"  >
		       <ul  class="nav comment-ul" id="comm'.$post->get_id().'" >';
		       $user = (new User())->get_user($comments->get_user_id());
		       if(!($user->get_id() == $_SESSION['user_id'])){
		         $commenter = $user->get_fullName();
		       }
		       else{
		        $commenter = 'Me';
		        }
		     $output .='
		      <li class="comment">
			   <div class="row">
			   <div class="comment_image">
			     <img class="img col-lg-1 col-md-1 col-sm-1 col-xs-1 comment_img"     src="../../pub/img/userImages/'.$user->get_profile_picture().'" >
			   </div>
			   <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
			       <h6 class="comment_head">'.$commenter.' commented</h5>
			       <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
			     <p class="comment_content">
			      '.$comments->get_comment().'
			     </p>
			     </div>
			   </div>
			   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                             <span style="font-size:50%;">
                            '.$displayDate.'
                            </span>
                            </div>
			   </div>
			  
			   </li>
			   </ul>
		    </div>
		    </div>';
		}
		}
		
		
		$output .='
	       <div class="dropdown" id="dropdown'.$post->get_id().'" >
	       <ul class="nav nav-pills content_nav comment-ul">
		 <li class="dropdown-toggle text_nav" data-toggle="dropdown" data-value="'.$post->get_id().'">
		   <a href="#" >
		   <span class="glyphicon glyphicon-comment"></span> Comment
		 </a>
		 
		 </li>
		
		
		</ul>
		<ul class="nav" style="display:none;" id="drop'.$post->get_id().'" >
		 
		   <li>
		   <div class="col-md-6">
		   <textarea class="comment_content" name="comment_content" onfocus="this.value=null;this.onfocus=null;" >
		   </textarea>
		   <br>
		   <div class="fmessage">
		     <button class="btn btn-small pull-left comment" >Comment</button>
		     <span class="pull-right notification" id="notification'.$post->get_id().'" style="display:none;"></span>
		   </div>
		   </div>
	       
		   </li>
		   
	   
	        </ul>
	 
	   
	        
		</div>';
		
		$output .='
		
	      </div>';
	       (new Post())->updateSeen($post->get_id());
	        
	       }
              echo $output;
	      }else{
                echo '<h3> You have no posts </h3>';
              }
	       
	       
	       
	      ?>
	       
	 
	       
		<!-- All Stories list-->
		
	            </div>
			 </div><!-- end of row for story form -->

         </div><!-- end of col-md-6 -->
         <div class="col-md-3 visible-md visible-lg s_row">
            <?php
		try{
		  $template->render('left_side_menu.php');
		}
		catch(Exception $e){
		  echo 'Message: '. $e->getMessage();
		}

	      ?>
         </div><!-- end of col-md-3 -->

			 </div><!-- end u_main_content -->

			    </div>
		       </div>
		</div>
	       </div>
	</div>
	<div class="content">
	       <?php
		try{
		  $template->render('footer.php');
		}
		catch(Exception $e){
		  echo 'Message: '. $e->getMessage();
		}

	      ?>
	      </div>
	</div>
	<div class="hide" style="display:none;">
	 
	</div>
 </body>
</html>
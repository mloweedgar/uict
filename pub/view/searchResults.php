<?php

/* now it is only require_once in first index.php
require_once('./includes/services/Loader.php');
*/
$loader = new Loader();
$results = $data;

try{
   $loader->service('Template.php');
   $loader->service('CurrentPage.php');
}
catch(Exception $e){
 echo 'Message: '. $e->getMessage();
}


 CurrentPage::$currentPage = "searchResults";
 

$template = new Template();

// variable to detect the index page

?>

<!DOCTYPE html>
    <html lang='en'>
    <head>
        <meta charset="utf-8" />
        <title>UICT COMMUNITY</title>
        <?php
        try{
            $template->render('resources.php');
        }catch(Exception $e){
            echo 'Message'.$e->getMessage();
        }
         echo '<link rel="stylesheet" type="text/css"
	    href="../pub/css/events.css" />';
        ?>
            
    
              
                          
    </head>
        <body>
	 
	
	 
	
    <div id="page">
            <div id="header">
            <?php
              try{ $template->render('header.php');
              }
              catch(Exception $e){
                echo 'Message: '. $e->getMessage();
              }
            
            ?>
            </div>
            <div class="u_row container">
                <div class="results col-lg-offset-2 col-md-offset-2 col-sm-offset-2
		            col-xs-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-8">
                    <?php
                       if($results != NULL && (count($results["projects"]) != 0 || count($results["events"]) != 0)){
                        echo '<h3>Search results </h3>';
                        
                        foreach($results as $resultgroup){
			   foreach($resultgroup as $result){
			      if(isset($result['begin_date'])){
				 $date = $result['begin_date'];
			      }else if(isset($result['event_date'])){
				  $date = $result['event_date'];
			      }else{
				 $date = NULL;
			      }
                            echo '<div class="result">
                                   <div class="event">
                                   <div class="event-wrapper">
                                   <span class="event_tag"></span><span class="event_title"><a href="'.
                                       URL .'projects/community_project/'.urlencode($result['id']).'">
                                       '.$result['title'].'</a></span>
                                       <span class="event_tag">Description </span><span
                                       class="event_description">'.$result['description'].'</span>
                                       <span class="event_tag">Begin Date </span>
                                       <span class="event_time">'.$date.'</span>
                                      <span class="event_tag">Initiated By </span><span
                                      class="event_publisher">'.
                                       $result['first_name'].' '.$result['last_name'].'</span>
                                       </div>
                                   </div>
                                 </div>';
			    $result;
			   }
                        }
                        
                       }else{
                        echo '<h3> No search results</h3>';
                       }
                    
                    ?>
                    
                </div>
            </div>
   
          <!-- content -->
                      <div class="content">
                      <?php
                          try{
                             $template->render('footer.php');
                          }catch(Exception $e){
                             echo "Message: ".$e->getMessage();
                          }
                      ?>
                    </div>  
            
    </div>
        </body>
    </html>
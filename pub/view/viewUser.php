<?php $loader = new Loader();
   if(isset($data)){
    $user = $data['user'];
    }
    try{
    $loader->service('Template.php');
    $loader->service('CurrentPage.php');
    }
    catch(Exception $e){
     echo 'Message: '. $e->getMessage();
    }
    
     $userPosts = array();
         if(isset($data['posts'])){
         foreach($data['posts'] as $post){
            if($post->get_user_id() == $user->get_id()){
              array_push($userPosts,$post);
            }
         }
         }

CurrentPage::$currentPage = "profileview";
$template = new Template();

?>
<!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
                <title>Home | UICT Community</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
         <?php
                  try{
                      $template->render('resources.php');
                  }catch(Exception $e){
                      echo 'Message'.$e->getMessage();
                  }
           ?>
             
 <body>
        <div id="page"> <div id="header">
              <?php
                try{
                  $template->render('header.php');
                } catch(Exception $e){
                  echo 'Message: '. $e->getMessage();
                }
              
              ?> </div>
        <div class="container">
         <div class="row">
                 <h2
                   title="" >
                   <?php
                   echo $user->get_first_name().' Profile';
                   ?>
                   </h2>
         </div>
         <div class="row u_row">
            
         <div class="col-md-3 visible-md visible-lg">
                  
             <div class="row user_photo">
              <?php if($data['user']->get_profile_picture() != NULL){
                  echo '<img class="img img-thumbnail"
                  src="../../pub/img/userImages/'.$data['user']->get_profile_picture().'"
                  />';
              }else{
                      echo '<img class="img img-thumbnail"
                      src="../../pub/img/avatars/profileImage.jpg" />';
              } ?>
                         
                 </div>
             <!-- end of row for profile picture -->
                <!-- end of row for info -->
                <div class="col-sm-12 col-xs-12 visible-sm visible-xs">
	    <?php
		    try{
		     $dataToTemp = array(
					 'posts' =>$data['posts'],
					 'user' => $data['user']
					 );
		     $template->render('navigation_for_small.php',$dataToTemp);
		    }catch(Exception $e){
		     echo 'Message:'.$e->getMessage();
		    }
		  ?>
	 </div>
                <div class="row user_nav">
                  
                    <a href="<?php echo URL.'user/posts/'.$user->get_id();
                  ?>"
                       <?php echo 'class="list-group-item
                       '.(CurrentPage::$currentPage == "posts"?'active':'').'"';
                       ?>>
                    <span class="glyphicon glyphicon-comment"></span> Posts
                    <span class="post_number pull-right">
                    <?php echo
                    count($userPosts);?></span>
                    </a>
                 </div>
                 <div class="">
          <div class="list-group">
                                <h3>Events attended</h3>
               <ul class="nav">
                                       <li class="activity_li">3rd July 2014:
                                       <a>Orhanage visit</a></li> <li
                                       class="activity_li">1st May 2014:
                                       <a>Friendly match</a></li> <li
                                       class="activity_li">22nd April 2013:
                                       <a>Muhimbili Hospital Visit</a></li> <li
                                       class="activity_li">More than year ago:
                                       <a>Football match</a></li>
                                       
                                </ul> <div class="edit_button">
                         </div>
                       </div> <!-- end of col-md-3 -->

                         </div>

         </div><!-- end of col-md-3 --> <div class="col-md-6">
              <div class="row">
               <div class="col-lg-12">
                  <div class="input-group">
                    <input type="text" id="searchIn" class="form-control
                    searchIn" placeholder="Search for member"> <span
                    class="input-group-btn">
                      <button class="btn u_s_button searchbuttonHeight" type="button">Search <span
                      class="glyphicon glyphicon-search"></span></button>
                    </span>
                  </div><!-- /input-group -->
                </div><!-- /.col-lg-12 --> <div class="col-lg-12 searchResult"
                id="sResult">
                  <div class="users">
                     <ul class="nav" id="resultUl">
                     
                     </ul>
                  </div>
                  
                </div>
                <div class="col-lg-12" >
                <?php
                echo '<div class="dropdown list-group-item view_row">
            <ul class="nav nav-pills content_nav ">
	   <li class="dropdown-toggle text_nav col-lg-12 " data-toggle="dropdown" data-value="'.$user->get_id().'">
	     <a href="#" >
	     Send Message to '. $user->get_first_name().'
	   </a>
	   
	   </li>
	  
          </ul>
	  <ul class="nav" style="display:none;" id="drop'.$user->get_id().'" >
	   
	     <li>
	     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
	     <textarea class="message_content col-lg-12" name="message_content" onfocus="this.value=null;this.onfocus=null;" >
	     </textarea><br>
	     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	       <button class="btn btn-small pull-left send"  >Send</button>
	       <span class="notification pull-right " id="notification'.$user->get_id().'" style="display:none;"></span>
	     </div>
	     </div>
	 
	     </li>
	     
	   
	  </ul>
	 
	   
	   </div>';
           ?>
                </div>
       </div><!-- end of row for search bar -->
        <div class="row user_form">
           <!-- All Events list-->
           <div class="col-md-6">
           <div class="story_list custom_list">
               <h3 class="head-h3">Basic</h3>
                <ul class="nav">
                    <li class="activity_li">FullName:<a><?php echo
                      $user->get_first_name().' '.$user->get_last_name() ;?></a></li>
                      <li class="activity_li">Registration Number: <a><?php echo
                      $user->get_reg_number();?></a></li>
                      <li
                      class="activity_li">Degree program:
                      <a> <?php echo $user->get_programObj()->get_program_name();?></a></li>
                </ul>
           </div>
           </div>
           <div class="col-md-6">
           <div class="story_list custom_list">
              <h3 class="head-h3" >Contact</h3>
                  <ul class="nav">
                      <li class="activity_li">Email: <a><?php
                      echo $user->get_email();?></a></li>
                      <li
                      class="activity_li">Phone Number: <a><?php echo
                      $user->get_phonenumber(); ?></a></li>
                      <li
                      class="activity_li">Mailing Address: <a><?php echo
                      $user->get_mailing_address(); ?></a></li>
                  </ul>
           </div>
           </div>
           <div class="col-md-6">
           <div class="story_list custom_list">
              <h3 class="head-h3" >Personal info</h3>
                  <ul class="nav">
                      <li
                      class="activity_li">Status:<a><?php echo
                      $user->get_status(); ?></a></li>
                      <li
                      class="activity_li">Grad Year: <a><?php echo
                      $user->get_grad_year(); ?></a></li>
                     
               </ul>
           </div>
           </div>
           <div class="col-md-6">
           <div class="story_list custom_list">
              <h3 class="head-h3" >Others</h3>
                  <ul class="nav">
                      <li
                      class="activity_li">Skills: <a><?php echo
                      $user->get_skills(); ?></a></li>
                      <li
                      class="activity_li">Hobbies:<a><?php echo
                      $user->get_hobbies(); ?></a></li>
                     
               </ul>
           </div>
           </div>



        </div><!-- end of row for user form -->

         </div><!-- end of col-md-6 -->
         <!-- end u_main_content -->

                            </div>
                       </div>
                </div>
               </div>
        </div> <div class="content">
               <?php
                try{
                  $template->render('footer.php');
                } catch(Exception $e){
                  echo 'Message: '. $e->getMessage();
                }
              
              ?> </div>
        </div>
 </body>

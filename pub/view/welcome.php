<?php 
  /*
   require_once('../includes/model/session.php');
   require_once('../includes/helper/functions.php');

   if($session->is_logged_in == false){
       redirect('login.php');
   }
   */
?>
<?php
$loader = new Loader();

try{
$loader->service('Template.php');
$loader->service('CurrentPage.php');
}
catch(Exception $e){
 echo 'Message: '. $e->getMessage();
}

$template = new Template();

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Home | UICT Community</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">
           <?php
		try{
		    $template->render('resources.php');
		}catch(Exception $e){
		    echo 'Message'.$e->getMessage();
		}
	    ?>
           
 <body>
	<div id="page">
	<div id="header">
	      <?php
		try{
		  $template->render('header.php');
		}
		catch(Exception $e){
		  echo 'Message: '. $e->getMessage();
		}
	      
	      ?>
	    </div>

	<!-- end of header-->
	<div class="u_welcome_row container">
	<div class="welcome">
	<div class="row">
	 <div class="welcome_left col-lg-6 col-md-6 col-sm-6 col-xs-6">
	  <div class="row">
	   <div class="wel_note">
	    <span>Welcome</span>
	   </div>
	    
	  </div>
	  <div class="row">
	   <div class="wel_note" style="margin-left:37%;">
	    <span>to</span>
	   </div>
	    
	  </div>
	  <div class="row">
	   <div class="wel_note" style="margin-left:35%;">
	    <span>UICT</span>
	   </div>
	    
	  </div>
	  <div class="row">
	   <div class="wel_note" style="margin-left:25%;">
	    <span>Community</span>
	   </div>
	    
	  </div>
	  
	 </div>
	 <div class="welcome_right col-lg-6 col-md-6 col-sm-6 col-xs-6">
	  <div class="row">
	  <img src="
	    <?php
	    if($data->get_profile_picture() != NULL){
	    echo '../pub/img/userImages/'.$data->get_profile_picture();
	    }else{
	     echo '../pub/img/avatars/profileImage.jpg';
	    }
	    ?>" class="profIcon" alt="profile Icon" 
	    />
	    </div>
	    <div class="row s_row">
	    <a href="<?php echo URL.'home/userhome/'.$data->get_id() ?>"
			   class="u_button u_button_welcome" >View Account</a>
	     </div>
           
	 </div>
	</div>
	</div>
	</div>

	<!-- start of a footer-->
	<div class="content">
	       <?php
		try{
		  $template->render('footer.php');
		}
		catch(Exception $e){
		  echo 'Message: '. $e->getMessage();
		}
	      
	      ?>
	      </div>
	
	</div>
	</body>
	</html>